#include "Libraries.h"
#include "Extern.h"

#pragma once

class Bullet
{
	float X, Y, Z, XYangle, Zvector, Force;
	int Counter, Damage, Team, SmokeCounter, SmokeType;
public:
	Bullet(float x, float y, float z, float xyangle, float zvector, float force, int damage, int team, int smoketype)
	{
		X = x;
		Y = y;
		Z = z;
		XYangle = xyangle + 3.14/2;// + (rand()%51)/100.0 - 0.25;
		Zvector = zvector;// + (rand()%21)/100.0 - 0.1;
		Zvector = Zvector + (rand() % 4 - 1)/10.0;
		//Force = force * -sin((90-GetAngle(Zvector,force))*3.14/180);
		Force = force;
		Counter = 0;
		Damage = damage;
		Team = team;
		SmokeType = smoketype;
	}
	short Move()
	{
		Counter++;
		if (Counter > 350) return 1;
	
		//PHYSICS
		//if (Force > 0) Force -= 0.01;
		Zvector -= 0.03;

		X-=Force*cos(XYangle);
		Y-=Force*sin(XYangle);
		Z+=Zvector;

		if (X < 8) return 1;
		if (X >= MapX-8) return 1;
		if (Y < 8) return 1;
		if (Y >= MapY-8) return 1;
		if (VoxelMap[(int)X][(int)Y][(int)Z]==1)
		{
			Explosion();
			return 1;
		}
		SmokeCounter++;
		if (SmokeCounter >= 10)
		{
			SmokeCounter = 0;
			if (SmokeType == 1)
			return 2;
		}
		return 0;
	}
	void Draw()
	{
		if (X > -xPl+150 || X < -xPl-130 || Y > -yPl+150 || Y < -10) return;
		glTranslatef(X, Y, Z);
		if (Damage>=4)
			glPointSize(4.0f);
		else
			glPointSize(2.0f);
		glBegin(GL_POINTS);
		glColor3f(0.8f, 0.8f, 0.8f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glEnd();
		glTranslatef(-X, -Y, -Z);
		return;
	}
	void Explosion()
	{
		if (Damage <= 3)
		{
			VoxelMap[(int)X][(int)Y][(int)Z]=0;
		}
		if (Damage >= 4 && Damage <= 9)
		{
			VoxelMap[(int)X][(int)Y][(int)Z]=0;
			VoxelMap[(int)X+1][(int)Y][(int)Z]=0;
			VoxelMap[(int)X+2][(int)Y][(int)Z]=0;
			VoxelMap[(int)X-1][(int)Y][(int)Z]=0;
			VoxelMap[(int)X-2][(int)Y][(int)Z]=0;
			VoxelMap[(int)X][(int)Y+1][(int)Z]=0;
			VoxelMap[(int)X][(int)Y+2][(int)Z]=0;
			VoxelMap[(int)X][(int)Y-1][(int)Z]=0;
			VoxelMap[(int)X][(int)Y-2][(int)Z]=0;
			VoxelMap[(int)X][(int)Y][(int)Z+1]=0;
			VoxelMap[(int)X][(int)Y][(int)Z+2]=0;
			VoxelMap[(int)X][(int)Y][(int)Z-1]=0;
			VoxelMap[(int)X][(int)Y][(int)Z-2]=0;
			VoxelMap[(int)X+1][(int)Y+1][(int)Z+1]=0;
			VoxelMap[(int)X-1][(int)Y+1][(int)Z+1]=0;
			VoxelMap[(int)X+1][(int)Y-1][(int)Z+1]=0;
			VoxelMap[(int)X+1][(int)Y-1][(int)Z+1]=0;
			VoxelMap[(int)X+1][(int)Y+1][(int)Z]=0;
			VoxelMap[(int)X-1][(int)Y+1][(int)Z]=0;
			VoxelMap[(int)X+1][(int)Y-1][(int)Z]=0;
			VoxelMap[(int)X+1][(int)Y-1][(int)Z]=0;
			VoxelMap[(int)X+1][(int)Y+1][(int)Z-1]=0;
			VoxelMap[(int)X-1][(int)Y+1][(int)Z-1]=0;
			VoxelMap[(int)X+1][(int)Y-1][(int)Z-1]=0;
			VoxelMap[(int)X+1][(int)Y-1][(int)Z-1]=0;
		}
		if (Damage >= 10)
		{
			for (int x = (int)X-7; x <= (int)X+7; x++)
			{
				for (int y = (int)Y-7; y <= (int)Y+7; y++)
				{
					for (int z = (int)Z-6; z <= (int)Z+6; z++)
					{
						if (abs((x-X)*(x-X) + (y-Y)*(y-Y) + (z-Z)*(z-Z)*2) <= 36)
						VoxelMap[x][y][z] = 0;
					}
				}
			}
		}
	}
	int GetX()
	{
		int x = (int) X;
		return x;
	}
	int GetY()
	{
		int y = (int) Y;
		return y;
	}
	int GetZ()
	{
		int z = (int) Z;
		return z;
	}
	float GetFloatX()
	{
		return X;
	}
	float GetFloatY()
	{
		return Y;
	}
	float GetFloatZ()
	{
		return Z;
	}
	int GetDamage()
	{
		return Damage;
	}
	int GetCounter()
	{
		return Counter;
	}
	int GetTeam()
	{
		return Team;
	}
};