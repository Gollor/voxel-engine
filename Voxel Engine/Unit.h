#include "Libraries.h"

#pragma once

#define GL_PI 3.1415f

class Unit
{
public:
	int X, Y, Z;
	float XStep, YStep;
	bool Selected;
	virtual bool Process() = 0;
	virtual void Move(int x, int y) = 0;
	virtual void Targeting(int x, int y, int z, float xstep, float ystep) = 0;
	virtual void ClearTarget() = 0;
	virtual void Draw() = 0;
	virtual void Hit(int x, int y, int z, int damage) = 0;
	virtual int GetHeight() = 0;
	virtual int GetYSize() = 0;
	virtual int GetXSize() = 0;
	int GetX()
	{
		return X;
	}
	int GetY()
	{
		return Y;
	}
	int GetZ()
	{
		return Z;
	}
	float GetYStep()
	{
		return YStep;
	}
	float GetXStep()
	{
		return XStep;
	}
	void Select()
	{
		Selected = 1;
	}
	void DisSelect()
	{
		Selected = 0;
	}
};