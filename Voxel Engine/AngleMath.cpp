#include "Libraries.h"
#include "Extern.h"

#pragma once

#define GL_PI 3.1415f

int GetAngle(float x, float y)
{
	float r = sqrt(x*x+y*y);
	int A = asin(y/r)*180.0/GL_PI;
	if (x>=0) A=A+90;
	else A=270-A;
	return A;
}