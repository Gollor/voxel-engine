#include "Libraries.h"
#include "Extern.h"
#include "Lists.h"

#pragma once

#define GL_PI 3.1415f

class Tank : public Unit
{
private:
	int Team;
	int RespectX, RespectY, TargetX, TargetY, TargetZ, FireCounter, FireCounter2, MoveCounter, HP;
	float BitX, BitY, TargetXStep, TargetYStep;
	bool VoxelScheme[12][12][12];
	float TextureScheme [10][10][10][3];
	float Angle, RespectAngle;
	bool UseWay;
	int MyWayEnd;
	short MyWay[1000][2];
public:
	Tank(int team, int x, int y, bool voxelScheme[10][10][10], float textureScheme [10][10][10][3])
	{
		Angle = 0;
		RespectAngle = 0;
		Team = team;
		X = x;
		Y = y;
		RespectX = x;
		RespectY = y;
		TargetX = 1000;
		TargetY = 1000;
		TargetXStep = 0;
		TargetYStep = 0;
		BitX = 0;
		BitY = 0;
		XStep = 0;
		YStep = 0;
		Selected = 0;
		FireCounter = 0;
		MoveCounter = 0;
		HP = 45;
		UseWay = 0;
		for (int a = 0; a < 10; a++) {
		for (int b = 0; b < 10; b++) {
		for (int c = 0; c < 10; c++) {
		VoxelScheme[a+1][b+1][c+1] = voxelScheme[a][b][c];
		TextureScheme[a][b][c][0] = textureScheme[a][b][c][0];
		TextureScheme[a][b][c][1] = textureScheme[a][b][c][1];
		TextureScheme[a][b][c][2] = textureScheme[a][b][c][2]; 
		} } }
	}
	bool Process()
	{
		if (HP <= 0)
		{
			for (int i = X-9; i <= X+9; i+=3)
			{
				for (int y = Y-9; y <= Y+9; y+=3)
				{
					OE_NewSpark(i, y, Z+4, 2, 2, 2);
					OE_NewSpark(i, y, Z+4, -2, 2, 2);
					OE_NewSpark(i, y, Z+4, -2, -2, 2);
					OE_NewSpark(i, y, Z+4, 2, -2, 2);
				}
			}
			for (int i = 1; i < 11; i++)
			{
				for (int j = 1; j < 11; j++)
				{
					for (int u = 1; u < 6; u++)
					{
						if (Angle >= 315 || Angle < 45)
						if (rand () % 3 != 0) 
						{
							VoxelMap[X+i-1][Y+j-1][Z+u-1] = VoxelScheme[i][j][u]; 
							TextureMap[X+i-1][Y+j-1][Z+u-1][0] = TextureScheme[i-1][j-1][u-1][0]; 
							TextureMap[X+i-1][Y+j-1][Z+u-1][1] = TextureScheme[i-1][j-1][u-1][1]; 
							TextureMap[X+i-1][Y+j-1][Z+u-1][2] = TextureScheme[i-1][j-1][u-1][2]; 
						}
						if (Angle >= 45 && Angle < 135)
						if (rand () % 3 != 0) 
						{
							VoxelMap[X+9-j][Y+9-i][Z+u-1] = VoxelScheme[i][j][u]; 
							TextureMap[X+9-j][Y+9-i][Z+u-1][0] = TextureScheme[i-1][j-1][u-1][0]; 
							TextureMap[X+9-j][Y+9-i][Z+u-1][1] = TextureScheme[i-1][j-1][u-1][1]; 
							TextureMap[X+9-j][Y+9-i][Z+u-1][2] = TextureScheme[i-1][j-1][u-1][2]; 
						}
						if (Angle >= 135 && Angle < 225)
						if (rand () % 3 != 0) 
						{
							VoxelMap[X+9-i][Y+9-j][Z+u-1] = VoxelScheme[i][j][u]; 
							TextureMap[X+9-i][Y+9-j][Z+u-1][0] = TextureScheme[i-1][j-1][u-1][0]; 
							TextureMap[X+9-i][Y+9-j][Z+u-1][1] = TextureScheme[i-1][j-1][u-1][1]; 
							TextureMap[X+9-i][Y+9-j][Z+u-1][2] = TextureScheme[i-1][j-1][u-1][2]; 
						}
						if (Angle >= 225 && Angle < 315)
						if (rand () % 3 != 0) 
						{
							VoxelMap[X+j-1][Y+i-1][Z+u-1] = VoxelScheme[i][j][u]; 
							TextureMap[X+j-1][Y+i-1][Z+u-1][0] = TextureScheme[i-1][j-1][u-1][0]; 
							TextureMap[X+j-1][Y+i-1][Z+u-1][1] = TextureScheme[i-1][j-1][u-1][1]; 
							TextureMap[X+j-1][Y+i-1][Z+u-1][2] = TextureScheme[i-1][j-1][u-1][2]; 
						}
					}
				}
			}
			if (Selected)
			{	
				Unit *pUnit;
				UnitListIterator iterator1 = UnitListSelect.begin(); 
				while(iterator1 != UnitListSelect.end())
				{
					pUnit = *iterator1;
					if(pUnit==this)
					{
						iterator1 = UnitListSelect.erase(iterator1);
						return 1;
					}
					else
					iterator1++;
				}
			}
			return 1;
		}
		if (TargetX != 1000)
		{
			if (FireCounter >= 75)
			{
				float r =  sqrt((float)(TargetX*TargetX + TargetY*TargetY));
				if (r < 300)
				{
					float A = GetAngle(TargetX + r*TargetXStep/4.0, TargetY + r*TargetYStep/4.0);
					OE_NewBullet(X + 5, Y + 5, Z + 6, A*3.14/180, -0.12+0.03*(r/8.0) + 4.0*TargetZ/r, 4, 5, Team, 1);
				}
				FireCounter = 0;
			}
			if (FireCounter2 >= 25)
			{
				float r =  sqrt((float)(TargetX*TargetX + TargetY*TargetY));
				if (r < 100)
				{
					OE_NewBullet(X + 5, Y + 5, Z + 4, GetAngle(TargetX,TargetY)*3.14/180, -0.21+0.03*r/6.0, 3, 2, Team, 0);
				}
				FireCounter2 = 0;
			}
		}
		Angle = (Angle*4+RespectAngle)/5;
		BitX += XStep;
		BitY += YStep;
		if (BitX >= 1.0) {X++; BitX-=1.0f;}
		if (BitX <= -1.0) {X--; BitX+=1.0f;}
		if (BitY >= 1.0) {Y++; BitY-=1.0f;}
		if (BitY <= -1.0) {Y--; BitY+=1.0f;}
		for (int i = 80; i > 1; i--)
		{
			if (VoxelMap[X+5][Y+5][i] == 1)
			{
				Z = i+1;
				break;
			}
		}
		if (RespectX + 2 > X && RespectX - 2 < X && RespectY + 2 > Y && RespectY - 2 < Y)
		{
			if (Team==2)
			{
				if (UseWay == 0)
				{
					UseWay = 1;
					MyWayEnd = 0;
					MyWay[0][0] = rand() % (MapX-20)+10;
					MyWay[0][1] = rand() % (MapY-20)+10;
				}
				RespectAngle = GetAngle(MyWay[MyWayEnd][0]-X,MyWay[MyWayEnd][1]-Y);
				RespectX=MyWay[MyWayEnd][0];
				RespectY=MyWay[MyWayEnd][1];
				MyWayEnd--;
				if (MyWayEnd < 0) UseWay = 0;
				float r = (RespectX-X)*(RespectX-X) + (RespectY-Y)*(RespectY-Y);
				float a = 0.5 / sqrt(r);
				XStep = a * (RespectX-X);
				YStep = a * (RespectY-Y);
			}
			else
			{
				XStep = 0;
				YStep = 0;
			}
		}
		FireCounter++;
		FireCounter2++;
		return 0;
	}
	void Move(int x, int y)
	{
		UseWay = 1;
		MyWayEnd = 0;
		MyWay[0][0] = x;
		MyWay[0][1] = y;
		RespectAngle = GetAngle(MyWay[MyWayEnd][0]-X,MyWay[MyWayEnd][1]-Y);
		RespectX=MyWay[MyWayEnd][0];
		RespectY=MyWay[MyWayEnd][1];
		MyWayEnd--;
		if (MyWayEnd < 0) UseWay = 0;
		float r = (RespectX-X)*(RespectX-X) + (RespectY-Y)*(RespectY-Y);
		float a = 0.5 / sqrt(r);
		XStep = a * (RespectX-X);
		YStep = a * (RespectY-Y);
	}
	void Targeting(int x, int y, int z, float xstep, float ystep)
	{
		static Unit *pUnit = 0;
		if ((x-X)*(x-X)+(y-Y)*(y-Y) < TargetX*TargetX+TargetY*TargetY)
		{
			TargetX = x-X;
			TargetY = y-Y;
			TargetZ = z-2-Z-4;
			TargetXStep = xstep;
			TargetYStep = ystep;
		}
	}
	void ClearTarget()
	{
		TargetX = 1000;
		TargetY = 1000;
	}
	void Draw()
	{
		if (X > -xPl+150 || X < -xPl-130 || Y > -yPl+150 || Y < -10) return;
		glTranslatef(X+5.0f+BitX, Y+5.0f+BitY, Z);
		glRotatef(Angle, 0.0f, 0.0f, 1.0f);
		glBegin(GL_QUADS);
		for(int x = 0; x < 10; x++)
		{
			for (int y = 0; y < 10; y++)
			{
				for (int z = 0; z < 4; z++)
				{
					if (VoxelScheme[x+1][y+1][z+1] == 1)
					{
						if (Selected)
						glColor3f(TextureScheme[x][y][z][0]+0.2, TextureScheme[x][y][z][1]+0.2, TextureScheme[x][y][z][2]+0.2);
						else
						glColor3f(TextureScheme[x][y][z][0], TextureScheme[x][y][z][1], TextureScheme[x][y][z][2]);
						if (VoxelScheme[x+1][y+1][z+2] != 1 || z == 3)
						{
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
						}
						if (VoxelScheme[x+2][y+1][z+1] != 1)
						{
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z-0.5f);
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z-0.5f);
						}
						if (VoxelScheme[x][y+1][z+1] != 1)
						{
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z-0.5f);
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z-0.5f);
						}
						if (VoxelScheme[x+1][y+2][z+1] != 1)
						{
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z-0.5f);
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z-0.5f);
						}
						if (VoxelScheme[x+1][y][z+1] != 1)
						{
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z-0.5f);
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z-0.5f);
						}
					}
				}
			}
		}
		glEnd();
		glRotatef(360-Angle, 0.0f, 0.0f, 1.0f);
		glRotatef(GetAngle(TargetX,TargetY), 0.0f, 0.0f, 1.0f);
		glBegin(GL_QUADS);
		for(int x = 0; x < 10; x++)
		{
			for (int y = 0; y < 10; y++)
			{
				for (int z = 4; z < 10; z++)
				{
					if (VoxelScheme[x+1][y+1][z+1] == 1)
					{
						if (Selected)
						glColor3f(TextureScheme[x][y][z][0]+0.2, TextureScheme[x][y][z][1]+0.2, TextureScheme[x][y][z][2]+0.2);
						else
						glColor3f(TextureScheme[x][y][z][0], TextureScheme[x][y][z][1], TextureScheme[x][y][z][2]);
						if (VoxelScheme[x+1][y+1][z+2] != 1)
						{
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
						}
						if (VoxelScheme[x+2][y+1][z+1] != 1)
						{
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z-0.5f);
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z-0.5f);
						}
						if (VoxelScheme[x][y+1][z+1] != 1)
						{
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z-0.5f);
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z-0.5f);
						}
						if (VoxelScheme[x+1][y+2][z+1] != 1)
						{
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z+0.5f);
							glVertex3f(x-0.5f-5.0f, y+0.5f-5.0f, z-0.5f);
							glVertex3f(x+0.5f-5.0f, y+0.5f-5.0f, z-0.5f);
						}
						if (VoxelScheme[x+1][y][z+1] != 1)
						{
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z+0.5f);
							glVertex3f(x+0.5f-5.0f, y-0.5f-5.0f, z-0.5f);
							glVertex3f(x-0.5f-5.0f, y-0.5f-5.0f, z-0.5f);
						}
					}
				}
			}
		}
		glEnd();
		glRotatef(360-GetAngle(TargetX,TargetY), 0.0f, 0.0f, 1.0f);
		glTranslatef(-X-5.0f-BitX, -Y-5.0f-BitY, -Z);
	}
	void Hit(int x, int y, int z, int damage)
	{
		HP-=damage;
		int xx = rand() % 6 + 3;
		int yy = rand() % 10 + 1;
		int zz = rand() % 8 + 1;
		if (damage>=4)
		{
			VoxelScheme[xx][yy][zz]=0;
			VoxelScheme[xx+1][yy][zz]=0;
			VoxelScheme[xx+2][yy][zz]=0;
			VoxelScheme[xx-1][yy][zz]=0;
			VoxelScheme[xx-2][yy][zz]=0;
			VoxelScheme[xx][yy+1][zz]=0;
			VoxelScheme[xx][yy+2][zz]=0;
			VoxelScheme[xx][yy-1][zz]=0;
			VoxelScheme[xx][yy-2][zz]=0;
			VoxelScheme[xx][yy][zz+1]=0;
			VoxelScheme[xx][yy][zz+2]=0;
			VoxelScheme[xx][yy][zz-1]=0;
			VoxelScheme[xx][yy][zz-2]=0;
			VoxelScheme[xx+1][yy+1][zz+1]=0;
			VoxelScheme[xx-1][yy+1][zz+1]=0;
			VoxelScheme[xx+1][yy-1][zz+1]=0;
			VoxelScheme[xx+1][yy-1][zz+1]=0;
			VoxelScheme[xx+1][yy+1][zz]=0;
			VoxelScheme[xx-1][yy+1][zz]=0;
			VoxelScheme[xx+1][yy-1][zz]=0;
			VoxelScheme[xx+1][yy-1][zz]=0;
			VoxelScheme[xx+1][yy+1][zz-1]=0;
			VoxelScheme[xx-1][yy+1][zz-1]=0;
			VoxelScheme[xx+1][yy-1][zz-1]=0;
			VoxelScheme[xx+1][yy-1][zz-1]=0;
		}
		else
			VoxelScheme[xx][yy][zz]=0;
	}
	int GetYSize()
	{
		return 10;
	}
	int GetXSize()
	{
		return 10;
	}
	int GetHeight()
	{
		return Z+7;
	}
};