#include "Libraries.h"
#include "Extern.h"

#pragma once

class Spark
{
	float X, Y, Z, Xmove, Ymove, Zmove;
	int Counter;
public:
	Spark(float x, float y, float z, float xmove, float ymove, float zmove)
	{
		X = x;
		Y = y;
		Z = z;
		Xmove = xmove/25.0f;
		Ymove = ymove/25.0f;
		Zmove = zmove/25.0f;
		Counter = 0;
	}
	bool Move()
	{
		Counter++;
		if (Counter >= 100) return 1;
		if (X <= 1) return 1;
		if (X >= MapX-1) return 1;
		if (Y <= 1) return 1;
		if (Y >= MapY-1) return 1;
		X+=Xmove;
		Y+=Ymove;
		Z+=Zmove;
		return 0;
	}
	void Draw()
	{
		if (X > -xPl+150 || X < -xPl-130 || Y > -yPl+150 || Y < -10) return;
		glTranslatef(X, Y, Z);
		if (Counter < 25)
		glPointSize(8.0f);
		else if (Counter >= 25 && Counter < 50)
		glPointSize(4.0f);
		else if (Counter >= 50 && Counter < 75)
		glPointSize(2.0f);
		else if (Counter >= 75)
		glPointSize(1.0f);
		glBegin(GL_POINTS);
		glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glEnd();
		glTranslatef(-X, -Y, -Z);
		return;
	}
};