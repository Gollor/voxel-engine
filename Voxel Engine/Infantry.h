#include "Libraries.h"
#include "Extern.h"
#include "Lists.h"

#pragma once

#define GL_PI 3.1415f

class Infantry : public Unit
{
private:
	int RespectX, RespectY, TargetX, TargetY, TargetZ, FireCounter, MoveCounter, HP;
	float BitX, BitY, TargetXStep, TargetYStep;
	bool UseWay;
	int MyWayEnd;
	short MyWay[1000][2];
	int Team;
public:
	Infantry(int x, int y, int team)
	{
		X = x;
		Y = y;
		RespectX = x;
		RespectY = y;
		TargetX = 1000;
		TargetY = 1000;
		TargetXStep = 0;
		TargetYStep = 0;
		BitX = 0;
		BitY = 0;
		XStep = 0;
		YStep = 0;
		Selected = 0;
		FireCounter = 0;
		MoveCounter = 0;
		HP = 5;
		UseWay = 0;
		Team = team;
	}
	bool Process()
	{
		if (HP <= 0)
		{
			if (Selected)
			{	
				Unit *pUnit;
				UnitListIterator iterator1 = UnitListSelect.begin(); 
				while(iterator1 != UnitListSelect.end())
				{
					pUnit = *iterator1;
					if(pUnit==this)
					{
						iterator1 = UnitListSelect.erase(iterator1);
						return 1;
					}
					else
					iterator1++;
				}
			}
			return 1;
		}
		if (FireCounter >= 45)
		{
			if (TargetX != 1000)
			{
				float r =  sqrt((float)(TargetX*TargetX + TargetY*TargetY));
				if (r < 200)
				{
					int ra = rand() % HP;
					int xx, yy;
					if (ra==0) {xx=3; yy=3;}
					if (ra==1) {xx=0; yy=0;}
					if (ra==2) {xx=6; yy=0;}
					if (ra==3) {xx=6; yy=6;}
					if (ra==4) {xx=0; yy=6;}
					float A = GetAngle(TargetX + r*TargetXStep/3.0, TargetY + r*TargetYStep/3.0);
					OE_NewBullet(X + xx, Y + yy, Z + 3, A*3.14/180, -0.06+0.03*(r/6.0) + 3.0*TargetZ/r, 3, 2, Team, 0);
				}
			}
			FireCounter = 0;
		}
		BitX += XStep;
		BitY += YStep;
		if (BitX >= 1.0) {X++; BitX-=1.0f;}
		if (BitX <= -1.0) {X--; BitX+=1.0f;}
		if (BitY >= 1.0) {Y++; BitY-=1.0f;}
		if (BitY <= -1.0) {Y--; BitY+=1.0f;}
		for (int i = 28; i > 18; i--)
		{
			if (VoxelMap[X+5][Y+5][i] == 1)
			{
				Z = i+1;
				break;
			}
		}
		if (RespectX + 2 > X && RespectX - 2 < X && RespectY + 2 > Y && RespectY - 2 < Y)
		{
			if (Team==2)
			{
				if (UseWay == 0)
				{
					UseWay = 1;
					MyWayEnd = 0;
					MyWay[0][0] = rand() % (MapX-20)+10;
					MyWay[0][1] = rand() % (MapY-20)+10;
				}
				RespectX=MyWay[MyWayEnd][0];
				RespectY=MyWay[MyWayEnd][1];
				MyWayEnd--;
				if (MyWayEnd < 0) UseWay = 0;
				float r = (RespectX-X)*(RespectX-X) + (RespectY-Y)*(RespectY-Y);
				float a = 0.5 / sqrt(r);
				XStep = a * (RespectX-X);
				YStep = a * (RespectY-Y);
			}
			else
			{
				XStep = 0;
				YStep = 0;
			}
		}
		FireCounter+=HP;
		return 0;
	}
	void Move(int x, int y)
	{
		UseWay = 1;
		MyWayEnd = 0;
		MyWay[0][0] = x;
		MyWay[0][1] = y;
		RespectX=MyWay[MyWayEnd][0];
		RespectY=MyWay[MyWayEnd][1];
		MyWayEnd--;
		if (MyWayEnd < 0) UseWay = 0;
		float r = (RespectX-X)*(RespectX-X) + (RespectY-Y)*(RespectY-Y);
		float a = 0.4 / sqrt(r);
		XStep = a * (RespectX-X);
		YStep = a * (RespectY-Y);
	}
	void Targeting(int x, int y, int z, float xstep, float ystep)
	{
		if ((x-X)*(x-X)+(y-Y)*(y-Y) < TargetX*TargetX+TargetY*TargetY)
		{
			TargetX = x-X;
			TargetY = y-Y;
			TargetZ = z-2-Z;
			TargetXStep = xstep;
			TargetYStep = ystep;
		}
	}
	void ClearTarget()
	{
		TargetX = 1000;
		TargetY = 1000;
	}
	void Draw()
	{
		if (X > -xPl+150 || X < -xPl-130 || Y > -yPl+150 || Y < -10) return;
		glTranslatef(X+3.0f+BitX, Y+3.0f+BitY, 0);
		glBegin(GL_QUADS);

		int x, y;
		for (int i = 0; i < HP; i++)
		{
			if (i == 0) {x = 3; y = 3;}
			if (i == 1) {x = 0; y = 0;}
			if (i == 2) {x = 6; y = 0;}
			if (i == 3) {x = 6; y = 6;}
			if (i == 4) {x = 0; y = 6;}
			for (int i = 80; i > 1; i--){
				if (VoxelMap[X+x][Y+y][i] == 1)
				{Z = i+1; break;}}
			for (int z = Z; z <= Z+2; z++)
			{
				if (z == Z) if (Selected != 1) glColor3f(0.2, 0.6, 0.5); else glColor3f(0.4, 0.8, 0.7);
				if (z == Z+1) if (Selected != 1) glColor3f(0.2, 0.8, 0.3); else glColor3f(0.4, 1.0, 0.5);
				if (z == Z+2) if (Team == 1) glColor3f(0, 0, 1); else glColor3f(1, 0, 0);

				if (X < -xPl)
				{
					glVertex3f(x+0.5f-3.0f, y-0.5f-3.0f, z+0.5f);
					glVertex3f(x+0.5f-3.0f, y+0.5f-3.0f, z+0.5f);
					glVertex3f(x+0.5f-3.0f, y+0.5f-3.0f, z-0.5f);
					glVertex3f(x+0.5f-3.0f, y-0.5f-3.0f, z-0.5f);
				}

				if (X > -xPl)
				{
					glVertex3f(x-0.5f-3.0f, y+0.5f-3.0f, z+0.5f);
					glVertex3f(x-0.5f-3.0f, y-0.5f-3.0f, z+0.5f);
					glVertex3f(x-0.5f-3.0f, y-0.5f-3.0f, z-0.5f);
					glVertex3f(x-0.5f-3.0f, y+0.5f-3.0f, z-0.5f);
				}
						
				/*glVertex3f(x+0.5f-3.0f, y+0.5f-3.0f, z+0.5f);
				glVertex3f(x-0.5f-3.0f, y+0.5f-3.0f, z+0.5f);
				glVertex3f(x-0.5f-3.0f, y+0.5f-3.0f, z-0.5f);
				glVertex3f(x+0.5f-3.0f, y+0.5f-3.0f, z-0.5f);*/
			
				glVertex3f(x-0.5f-3.0f, y-0.5f-3.0f, z+0.5f);
				glVertex3f(x+0.5f-3.0f, y-0.5f-3.0f, z+0.5f);
				glVertex3f(x+0.5f-3.0f, y-0.5f-3.0f, z-0.5f);
				glVertex3f(x-0.5f-3.0f, y-0.5f-3.0f, z-0.5f);
			}
			int z=Z+2;
			glVertex3f(x+0.5f-3.0f, y+0.5f-3.0f, z+0.5f);
			glVertex3f(x+0.5f-3.0f, y-0.5f-3.0f, z+0.5f);
			glVertex3f(x-0.5f-3.0f, y-0.5f-3.0f, z+0.5f);
			glVertex3f(x-0.5f-3.0f, y+0.5f-3.0f, z+0.5f);
		}

		

		glEnd();
		glTranslatef(-X-3.0f-BitX, -Y-3.0f-BitY, 0);
	}
	void Hit(int x, int y, int z, int damage)
	{
		HP--;
	}
	int GetYSize()
	{
		if (HP > 2)
		return 6;
		return 4;
	}
	int GetXSize()
	{
		if (HP > 1)
		return 6;
		return 4;
	}
	int GetHeight()
	{
		return Z+2;
	}
};