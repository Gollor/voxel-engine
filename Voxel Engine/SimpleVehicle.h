#include "Libraries.h"
#include "Extern.h"
#include "Lists.h"

#pragma once

#define GL_PI 3.1415f

class SimpleVehicle : public Unit
{
private:
	int Team;
	char Name[10];
	int RespectX, RespectY, TargetX, TargetY, FireCounter, FireCounter2, HP;
	float BitX, BitY, TargetXStep, TargetYStep;
	bool VoxelScheme[10][10][10];
	int ValueScheme[10][10][10];
	float TextureScheme[10][10][10][3];
	float GunScheme[7][2];
	float Angle, RespectAngle;
	bool UseWay;
	int MyWayEnd;
	short MyWay[1000][2];
	float Speed;
	int TurretHeight, EndX, EndY, EndZ;
public:
	SimpleVehicle(int team, int x, int y, char name[10], int endX, int endY, int endZ, bool voxelScheme[10][10][10], int valueScheme[10][10][10], float textureScheme [10][10][10][3], int gunScheme[7][2], int speed, int turretheight, int hp)
	{
		EndX = endX;
		EndY = endY;
		EndZ = endZ;
		Angle = 0;
		RespectAngle = 0;
		Team = team;
		X = x;
		Y = y;
		RespectX = x;
		RespectY = y;
		TargetX = 1000;
		TargetY = 1000;
		TargetXStep = 0;
		TargetYStep = 0;
		BitX = 0;
		BitY = 0;
		XStep = 0;
		YStep = 0;
		Selected = 0;
		FireCounter = 0;
		FireCounter2 = 0;
		HP = hp;
		Speed = speed;
		TurretHeight = turretheight;
		UseWay = 0;
		for (int i = 0; i < 7; i++)
		{
			GunScheme[i][0] = gunScheme[i][0]; //0 - X��������	1 - Y��������	2 - Z��������	3 - ����	4 - �������	5 - �����������	6 - ��������������������*100
			GunScheme[i][1] = gunScheme[i][1];
		}
		for (int a = 0; a < 10; a++) {
		Name[a] = name[a];
		for (int b = 0; b < 10; b++) {
		for (int c = 0; c < 10; c++) {
		VoxelScheme[a][b][c] = voxelScheme[a][b][c];
		ValueScheme[a][b][c] = valueScheme[a][b][c];
		TextureScheme[a][b][c][0] = textureScheme[a][b][c][0];
		TextureScheme[a][b][c][1] = textureScheme[a][b][c][1];
		TextureScheme[a][b][c][2] = textureScheme[a][b][c][2]; 
		} } }
	}
	bool Process()
	{
		if (HP <= 0)
		{
			for (int i = X-9; i <= X+9; i+=3)
			{
				for (int y = Y-9; y <= Y+9; y+=3)
				{
					OE_NewSpark(i, y, Z+4, 2, 2, 2);
					OE_NewSpark(i, y, Z+4, -2, 2, 2);
					OE_NewSpark(i, y, Z+4, -2, -2, 2);
					OE_NewSpark(i, y, Z+4, 2, -2, 2);
				}
			}
			for (int i = 0; i < EndX; i++)
			{
				for (int j = 0; j < EndY; j++)
				{
					for (int u = 0; u < EndZ; u++)
					{
						if (Angle >= 315 || Angle < 45)
						if (rand () % 3 != 0) 
						{
							VoxelMap[X+i][Y+j][Z+u] = VoxelScheme[i][j][u]; 
							TextureMap[X+i][Y+j][Z+u][0] = TextureScheme[i][j][u][0]; 
							TextureMap[X+i][Y+j][Z+u][1] = TextureScheme[i][j][u][1]; 
							TextureMap[X+i][Y+j][Z+u][2] = TextureScheme[i][j][u][2]; 
						}
						if (Angle >= 45 && Angle < 135)
						if (rand () % 3 != 0) 
						{
							VoxelMap[X+EndX-1-j][Y+EndY-1-i][Z+u] = VoxelScheme[i][j][u]; 
							TextureMap[X+EndX-1-j][Y+EndY-1-i][Z+u][0] = TextureScheme[i][j][u][0]; 
							TextureMap[X+EndX-1-j][Y+EndY-1-i][Z+u][1] = TextureScheme[i][j][u][1]; 
							TextureMap[X+EndX-1-j][Y+EndY-1-i][Z+u][2] = TextureScheme[i][j][u][2]; 
						}
						if (Angle >= 135 && Angle < 225)
						if (rand () % 3 != 0) 
						{
							VoxelMap[X+EndX-1-i][Y+EndY-1-j][Z+u] = VoxelScheme[i][j][u]; 
							TextureMap[X+EndX-1-i][Y+EndY-1-j][Z+u][0] = TextureScheme[i][j][u][0]; 
							TextureMap[X+EndX-1-i][Y+EndY-1-j][Z+u][1] = TextureScheme[i][j][u][1]; 
							TextureMap[X+EndX-1-i][Y+EndY-1-j][Z+u][2] = TextureScheme[i][j][u][2]; 
						}
						if (Angle >= 225 && Angle < 315)
						if (rand () % 3 != 0) 
						{
							VoxelMap[X+j][Y+i][Z+u] = VoxelScheme[i][j][u]; 
							TextureMap[X+j][Y+i][Z+u][0] = TextureScheme[i][j][u][0]; 
							TextureMap[X+j][Y+i][Z+u][1] = TextureScheme[i][j][u][1]; 
							TextureMap[X+j][Y+i][Z+u][2] = TextureScheme[i][j][u][2]; 
						}
					}
				}
			}
			if (Selected)
			{	
				Unit *pUnit;
				UnitListIterator iterator1 = UnitListSelect.begin(); 
				while(iterator1 != UnitListSelect.end())
				{
					pUnit = *iterator1;
					if(pUnit==this)
					{
						iterator1 = UnitListSelect.erase(iterator1);
						return 1;
					}
					else
					iterator1++;
				}
			}
			return 1;
		}
		if (TargetX != 1000)
		{
			if (FireCounter >= GunScheme[5][0])
			{
				float r =  sqrt((float)(TargetX*TargetX + TargetY*TargetY));
				if (r < 250*GunScheme[2][0]/GunScheme[6][0])
				{
					float a = 2.5 / r;
					float xStep = a * (TargetX+r*TargetXStep/2.5);
					float yStep = a * (TargetY+r*TargetYStep/2.5);
					OE_NewBullet(X + GunScheme[0][0], Y + GunScheme[1][0], Z + GunScheme[2][0], xStep + (rand()%4-2)/10, yStep + (rand()%4-2)/10, -0.05f, GunScheme[3][0], Team, GunScheme[4][0]);
				}
				FireCounter = 0;
			}
			if (FireCounter2 >= GunScheme[5][1])
			{
				float r =  sqrt((float)(TargetX*TargetX + TargetY*TargetY));
				if (r < 250*GunScheme[2][1]/GunScheme[6][1])
				{
					float a = 2.5 / r;
					float xStep = a * (TargetX+r*TargetXStep/2.5);
					float yStep = a * (TargetY+r*TargetYStep/2.5);
					OE_NewBullet(X + GunScheme[0][1], Y + GunScheme[1][1], Z + GunScheme[2][1], xStep + (rand()%4-2)/10, yStep + (rand()%4-2)/10, -0.05f, GunScheme[3][1], Team, GunScheme[4][1]);
				}
				FireCounter2 = 0;
			}
		}
		Angle = (Angle*4+RespectAngle)/5;
		BitX += XStep;
		BitY += YStep;
		if (BitX >= 1.0) {X++; BitX-=1.0f;}
		if (BitX <= -1.0) {X--; BitX+=1.0f;}
		if (BitY >= 1.0) {Y++; BitY-=1.0f;}
		if (BitY <= -1.0) {Y--; BitY+=1.0f;}
		for (int i = 28; i > 14; i--)
		{
			if (VoxelMap[X + EndX/2][Y + EndY/2][i] == 1)
			{
				Z = i+1;
				break;
			}
		}
		if (RespectX + 2 > X && RespectX - 2 < X && RespectY + 2 > Y && RespectY - 2 < Y)
		{
			if (Team==2)
			{
				if (UseWay == 0)
				{
					UseWay = 1;
					MyWayEnd = 0;
					MyWay[0][0] = rand() % (MapX-20)+10;
					MyWay[0][1] = rand() % (MapY-20)+10;
				}
				RespectAngle = GetAngle(MyWay[MyWayEnd][0]-X,MyWay[MyWayEnd][1]-Y);
				RespectX=MyWay[MyWayEnd][0];
				RespectY=MyWay[MyWayEnd][1];
				MyWayEnd--;
				if (MyWayEnd < 0) UseWay = 0;
				float r = (RespectX-X)*(RespectX-X) + (RespectY-Y)*(RespectY-Y);
				float a = Speed / sqrt(r);
				XStep = a * (RespectX-X);
				YStep = a * (RespectY-Y);
			}
			else
			{
				XStep = 0;
				YStep = 0;
			}
		}
		FireCounter++;
		FireCounter2++;
		return 0;
	}
	void Move(int x, int y)
	{
		UseWay = 1;
		MyWayEnd = 0;
		MyWay[0][0] = x;
		MyWay[0][1] = y;
		RespectAngle = GetAngle(MyWay[MyWayEnd][0]-X,MyWay[MyWayEnd][1]-Y);
		RespectX=MyWay[MyWayEnd][0];
		RespectY=MyWay[MyWayEnd][1];
		MyWayEnd--;
		if (MyWayEnd < 0) UseWay = 0;
		float r = (RespectX-X)*(RespectX-X) + (RespectY-Y)*(RespectY-Y);
		float a = 0.5 / sqrt(r);//SPEED
		XStep = a * (RespectX-X);
		YStep = a * (RespectY-Y);
	}
	void Targeting(int x, int y, float xstep, float ystep)
	{
		static Unit *pUnit = 0;
		if ((x-X)*(x-X)+(y-Y)*(y-Y) < TargetX*TargetX+TargetY*TargetY)
		{
			TargetX = x-X;
			TargetY = y-Y;
			TargetXStep = xstep;
			TargetYStep = ystep;
		}
	}
	void ClearTarget()
	{
		TargetX = 1000;
		TargetY = 1000;
	}
	void Draw()
	{
		if (X > -xPl+150 || X < -xPl-130 || Y > -yPl+150 || Y < -10) return;
		glTranslatef(X+EndX/2.0+BitX, Y+EndY/2.0+BitY, Z);
		glRotatef(Angle, 0.0f, 0.0f, 1.0f);
		glBegin(GL_QUADS);
		for(int x = 0; x < EndX; x++)
		{
			for (int y = 0; y < EndY; y++)
			{
				for (int z = 0; z < TurretHeight; z++)
				{
					if (VoxelScheme[x][y][z] == 1)
					{
						if (Selected)
						glColor3f(TextureScheme[x][y][z][0]+0.2, TextureScheme[x][y][z][1]+0.2, TextureScheme[x][y][z][2]+0.2);
						else
						glColor3f(TextureScheme[x][y][z][0], TextureScheme[x][y][z][1], TextureScheme[x][y][z][2]);
						if (VoxelScheme[x][y][z+1] != 1 || z == TurretHeight-1)
						{
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
						}
						if (VoxelScheme[x+1][y][z] != 1)
						{
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z-0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z-0.5f);
						}
						if (VoxelScheme[x-1][y][z] != 1)
						{
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z-0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z-0.5f);
						}
						if (VoxelScheme[x][y+1][z] != 1)
						{
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z-0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z-0.5f);
						}
						if (VoxelScheme[x][y-1][z] != 1)
						{
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z-0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z-0.5f);
						}
					}
				}
			}
		}
		glEnd();
		glRotatef(360-Angle, 0.0f, 0.0f, 1.0f);
		glRotatef(GetAngle(TargetX,TargetY), 0.0f, 0.0f, 1.0f);
		glBegin(GL_QUADS);
		for(int x = 0; x < EndX; x++)
		{
			for (int y = 0; y < EndY; y++)
			{
				for (int z = TurretHeight; z < EndZ; z++)
				{
					if (VoxelScheme[x][y][z] == 1)
					{
						if (Selected)
						glColor3f(TextureScheme[x][y][z][0]+0.2, TextureScheme[x][y][z][1]+0.2, TextureScheme[x][y][z][2]+0.2);
						else
						glColor3f(TextureScheme[x][y][z][0], TextureScheme[x][y][z][1], TextureScheme[x][y][z][2]);
						if (VoxelScheme[x][y][z+1] != 1)
						{
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
						}
						if (VoxelScheme[x+1][y][z] != 1)
						{
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z-0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z-0.5f);
						}
						if (VoxelScheme[x-1][y][z] != 1)
						{
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z-0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z-0.5f);
						}
						if (VoxelScheme[x][y+1][z] != 1)
						{
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y+0.5f-EndY/2.0, z-0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y+0.5f-EndY/2.0, z-0.5f);
						}
						if (VoxelScheme[x][y-1][z] != 1)
						{
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z+0.5f);
							glVertex3f(x+0.5f-EndX/2.0, y-0.5f-EndY/2.0, z-0.5f);
							glVertex3f(x-0.5f-EndX/2.0, y-0.5f-EndY/2.0, z-0.5f);
						}
					}
				}
			}
		}
		glEnd();
		glRotatef(360-GetAngle(TargetX,TargetY), 0.0f, 0.0f, 1.0f);
		glTranslatef(-X-EndX/2.0-BitX, -Y-EndY/2.0-BitY, -Z);
	}
	void Hit(int x, int y, int z, int damage)
	{
		HP-=damage;
		int xx = rand() % (EndX-2) + 1;
		int yy = rand() % (EndY-2) + 1;
		int zz = rand() % (EndZ-2) + 1;
		if (damage>=4)
		{
			VoxelScheme[xx][yy][zz]=0;
			VoxelScheme[xx+1][yy][zz]=0;
			VoxelScheme[xx+2][yy][zz]=0;
			VoxelScheme[xx-1][yy][zz]=0;
			VoxelScheme[xx-2][yy][zz]=0;
			VoxelScheme[xx][yy+1][zz]=0;
			VoxelScheme[xx][yy+2][zz]=0;
			VoxelScheme[xx][yy-1][zz]=0;
			VoxelScheme[xx][yy-2][zz]=0;
			VoxelScheme[xx][yy][zz+1]=0;
			VoxelScheme[xx][yy][zz+2]=0;
			VoxelScheme[xx][yy][zz-1]=0;
			VoxelScheme[xx][yy][zz-2]=0;
			VoxelScheme[xx+1][yy+1][zz+1]=0;
			VoxelScheme[xx-1][yy+1][zz+1]=0;
			VoxelScheme[xx+1][yy-1][zz+1]=0;
			VoxelScheme[xx+1][yy-1][zz+1]=0;
			VoxelScheme[xx+1][yy+1][zz]=0;
			VoxelScheme[xx-1][yy+1][zz]=0;
			VoxelScheme[xx+1][yy-1][zz]=0;
			VoxelScheme[xx+1][yy-1][zz]=0;
			VoxelScheme[xx+1][yy+1][zz-1]=0;
			VoxelScheme[xx-1][yy+1][zz-1]=0;
			VoxelScheme[xx+1][yy-1][zz-1]=0;
			VoxelScheme[xx+1][yy-1][zz-1]=0;
		}
		else
			VoxelScheme[xx][yy][zz]=0;
	}
	int GetYSize()
	{
		return EndY;
	}
	int GetXSize()
	{
		return EndX;
	}
	int GetHeight()
	{
		return Z+EndZ;
	}
};