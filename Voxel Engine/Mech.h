#include "Libraries.h"
#include "Extern.h"
#include "Lists.h"

#pragma once

#define GL_PI 3.1415f

class Mech : public Unit
{
private:
	int Team;
	int RespectX, RespectY, TargetX, TargetY, TargetZ, FireCounter, MoveCounter, HP;
	float BitX, BitY, TargetXStep, TargetYStep;
	bool VoxelScheme[5][5][6];
	float TextureScheme [3][3][4][3];
	float Angle, RespectAngle;
	bool UseWay;
	int MyWayEnd;
	short MyWay[1000][2];
public:
	Mech(int team, int x, int y)
	{
		Angle = 0;
		RespectAngle = 0;
		Team = team;
		X = x;
		Y = y;
		RespectX = x;
		RespectY = y;
		TargetX = 1000;
		TargetY = 1000;
		TargetXStep = 0;
		TargetYStep = 0;
		BitX = 0;
		BitY = 0;
		XStep = 0;
		YStep = 0;
		Selected = 0;
		FireCounter = 0;
		MoveCounter = 0;
		HP = 9;
		UseWay = 0;
		VoxelScheme[1][1][1] = 1;
		VoxelScheme[1][2][1] = 1;
		VoxelScheme[1][3][1] = 1;
		VoxelScheme[2][1][1] = 1;
		VoxelScheme[2][2][1] = 1;
		VoxelScheme[2][3][1] = 1;
		VoxelScheme[3][1][1] = 1;
		VoxelScheme[3][2][1] = 1;
		VoxelScheme[3][3][1] = 1;
		VoxelScheme[1][2][2] = 1;
		VoxelScheme[2][1][2] = 1;
		VoxelScheme[2][2][2] = 1;
		VoxelScheme[2][3][2] = 1;
		VoxelScheme[3][2][2] = 1;
		VoxelScheme[1][2][3] = 1;
		VoxelScheme[2][1][3] = 1;
		VoxelScheme[2][2][3] = 1;
		VoxelScheme[2][3][3] = 1;
		VoxelScheme[3][2][3] = 1;
		VoxelScheme[2][2][4] = 1;
		for (int a = 0; a < 3; a++) {
		for (int b = 0; b < 3; b++) {
		TextureScheme[a][b][0][0] = 0.3;
		TextureScheme[a][b][0][1] = 0.3;
		TextureScheme[a][b][0][2] = 0.3; 
		TextureScheme[a][b][1][0] = 0.5;
		TextureScheme[a][b][1][1] = 0.5;
		TextureScheme[a][b][1][2] = 0.5; 
		TextureScheme[a][b][2][0] = 0.5;
		TextureScheme[a][b][2][1] = 0.5;
		TextureScheme[a][b][2][2] = 0.5;
		} }
		TextureScheme[1][1][3][1] = 0.0;
		if (Team==1) {TextureScheme[1][1][3][0] = 0.0;TextureScheme[1][1][3][2] = 1.0;}
		else {TextureScheme[1][1][3][0] = 1.0;TextureScheme[1][1][3][2] = 0.0;}
	}
	bool Process()
	{
		if (HP <= 0)
		{
			for (int i = X-3; i <= X+3; i+=3)
			{
				for (int y = Y-3; y <= Y+3; y+=3)
				{
					OE_NewSpark(i, y, Z+4, 2, 2, 2);
					OE_NewSpark(i, y, Z+4, -2, 2, 2);
					OE_NewSpark(i, y, Z+4, -2, -2, 2);
					OE_NewSpark(i, y, Z+4, 2, -2, 2);
				}
			}
			if (Selected)
			{	
				Unit *pUnit;
				UnitListIterator iterator1 = UnitListSelect.begin(); 
				while(iterator1 != UnitListSelect.end())
				{
					pUnit = *iterator1;
					if(pUnit==this)
					{
						iterator1 = UnitListSelect.erase(iterator1);
						return 1;
					}
					else
					iterator1++;
				}
			}
			return 1;
		}
		if (TargetX != 1000)
		{
			if (FireCounter >= 33)
			{
				float r =  sqrt((float)(TargetX*TargetX + TargetY*TargetY));
				if (r < 200)
				{
					float A = GetAngle(TargetX + r*TargetXStep/3.0, TargetY + r*TargetYStep/3.0);
					OE_NewBullet(X + 1, Y + 1, Z + 4, A*3.14/180, -0.06+0.03*(r/6.0) + 3.0*TargetZ/r, 3, 4, Team, 0);
				}
				FireCounter = 0;
			}
		}
		Angle = (Angle*4+RespectAngle)/5;
		BitX += XStep;
		BitY += YStep;
		if (BitX >= 1.0) {X++; BitX-=1.0f;}
		if (BitX <= -1.0) {X--; BitX+=1.0f;}
		if (BitY >= 1.0) {Y++; BitY-=1.0f;}
		if (BitY <= -1.0) {Y--; BitY+=1.0f;}
		for (int i = 80; i > 1; i--)
		{
			if (VoxelMap[X+2][Y+2][i] == 1)
			{
				Z = i+1;
				break;
			}
		}
		if (RespectX + 2 > X && RespectX - 2 < X && RespectY + 2 > Y && RespectY - 2 < Y)
		{
			if (Team==2)
			{
				if (UseWay == 0)
				{
					UseWay = 1;
					MyWayEnd = 0;
					MyWay[0][0] = rand() % (MapX-20)+10;
					MyWay[0][1] = rand() % (MapY-20)+10;
				}
				RespectAngle = GetAngle(MyWay[MyWayEnd][0]-X,MyWay[MyWayEnd][1]-Y);
				RespectX=MyWay[MyWayEnd][0];
				RespectY=MyWay[MyWayEnd][1];
				MyWayEnd--;
				if (MyWayEnd < 0) UseWay = 0;
				float r = (RespectX-X)*(RespectX-X) + (RespectY-Y)*(RespectY-Y);
				float a = 0.5 / sqrt(r);
				XStep = a * (RespectX-X);
				YStep = a * (RespectY-Y);
			}
			else
			{
				XStep = 0;
				YStep = 0;
			}
		}
		FireCounter++;
		return 0;
	}
	void Move(int x, int y)
	{
		UseWay = 1;
		MyWayEnd = 0;
		MyWay[0][0] = x;
		MyWay[0][1] = y;
		RespectAngle = GetAngle(MyWay[MyWayEnd][0]-X,MyWay[MyWayEnd][1]-Y);
		RespectX=MyWay[MyWayEnd][0];
		RespectY=MyWay[MyWayEnd][1];
		MyWayEnd--;
		if (MyWayEnd < 0) UseWay = 0;
		float r = (RespectX-X)*(RespectX-X) + (RespectY-Y)*(RespectY-Y);
		float a = 0.5 / sqrt(r);
		XStep = a * (RespectX-X);
		YStep = a * (RespectY-Y);
	}
	void Targeting(int x, int y, int z, float xstep, float ystep)
	{
		static Unit *pUnit = 0;
		if ((x-X)*(x-X)+(y-Y)*(y-Y) < TargetX*TargetX+TargetY*TargetY)
		{
			TargetX = x-X;
			TargetY = y-Y;
			TargetZ = z-2-Z;
			TargetXStep = xstep;
			TargetYStep = ystep;
		}
	}
	void ClearTarget()
	{
		TargetX = 1000;
		TargetY = 1000;
	}
	void Draw()
	{
		if (X > -xPl+150 || X < -xPl-130 || Y > -yPl+150 || Y < -10) return;
		glTranslatef(X+1.0+BitX, Y+1.0+BitY, Z);
		glBegin(GL_QUADS);
		for(int x = 0; x < 3; x++)
		{
			for (int y = 0; y < 3; y++)
			{
				for (int z = 0; z < 4; z++)
				{
					if (VoxelScheme[x+1][y+1][z+1] == 1)
					{
						if (Selected)
						glColor3f(TextureScheme[x][y][z][0]+0.2, TextureScheme[x][y][z][1]+0.2, TextureScheme[x][y][z][2]+0.2);
						else
						glColor3f(TextureScheme[x][y][z][0], TextureScheme[x][y][z][1], TextureScheme[x][y][z][2]);
						if (VoxelScheme[x+1][y+1][z+2] != 1 || z == 1)
						{
							glVertex3f(x+0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
							glVertex3f(x+0.5f-1.0f, y-0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y-0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
						}
						if (VoxelScheme[x+2][y+1][z+1] != 1 && X < -xPl)
						{
							glVertex3f(x+0.5f-1.0f, y-0.5f-1.0f, z+0.5f);
							glVertex3f(x+0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
							glVertex3f(x+0.5f-1.0f, y+0.5f-1.0f, z-0.5f);
							glVertex3f(x+0.5f-1.0f, y-0.5f-1.0f, z-0.5f);
						}
						if (VoxelScheme[x][y+1][z+1] != 1 && X > -xPl)
						{
							glVertex3f(x-0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y-0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y-0.5f-1.0f, z-0.5f);
							glVertex3f(x-0.5f-1.0f, y+0.5f-1.0f, z-0.5f);
						}
						/*if (VoxelScheme[x+1][y+2][z+1] != 1)
						{
							glVertex3f(x+0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y+0.5f-1.0f, z-0.5f);
							glVertex3f(x+0.5f-1.0f, y+0.5f-1.0f, z-0.5f);
						}*/
						if (VoxelScheme[x+1][y][z+1] != 1)
						{
							glVertex3f(x-0.5f-1.0f, y-0.5f-1.0f, z+0.5f);
							glVertex3f(x+0.5f-1.0f, y-0.5f-1.0f, z+0.5f);
							glVertex3f(x+0.5f-1.0f, y-0.5f-1.0f, z-0.5f);
							glVertex3f(x-0.5f-1.0f, y-0.5f-1.0f, z-0.5f);
						}
					}
				}
			}
		}
		glEnd();
		glTranslatef(-X-1.0-BitX, -Y-1.0-BitY, -Z);
	}
	void Hit(int x, int y, int z, int damage)
	{
		HP-=damage;
		int xx = rand() % 3;
		int yy = rand() % 3;
		int zz = rand() % 4;
		VoxelScheme[xx][yy][zz]=0;
	}
	int GetYSize()
	{
		return 3;
	}
	int GetXSize()
	{
		return 3;
	}
	int GetHeight()
	{
		return Z+3;
	}
};