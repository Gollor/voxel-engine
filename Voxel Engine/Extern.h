#include "Libraries.h";
#include "Unit.h";

#pragma once

extern bool VoxelMap[400][400][200];
extern bool MoveMap[400][400];
extern float TextureMap[400][400][200][3];
extern GLfloat xPl;
extern GLfloat yPl;
extern GLfloat zPl;
extern int WayEnd;
extern short Way[1000][2];
extern int MapX, MapY;
extern int Money;

int GetAngle(float x, float y);
int ReadBmp(/*string*/char dir);
extern bool FindPath(int startx, int starty, int endx, int endy);

