#include "Libraries.h"
#include "Extern.h"
#include "Lists.h"

#pragma once

#define GL_PI 3.1415f

class Spacecraft
{
private:
	int Team, MoveCounter, X, Y, Z;
public:
	Spacecraft(int team, int x, int y)
	{
		Team = team;
		X = x;
		Y = y;
		Z = 80;
		MoveCounter = 0;
	}
	bool Process()
	{
		MoveCounter++;
		if (MoveCounter < 100 && MoveCounter % 2 == 0) Z--;
		if (MoveCounter > 200 && MoveCounter % 2 == 0) Z++;
		if (MoveCounter > 300) return 1;
		return 0;
	}
	void Draw()
	{
		if (X > -xPl+150 || X < -xPl-130 || Y > -yPl+150 || Y < -10) return;
		glTranslatef(X, Y, Z);
		glBegin(GL_QUADS);
		for(int x = 1; x < 9; x++)
		{
			for (int y = 1; y < 9; y++)
			{
				for (int z = 1; z < 9; z++)
				{
					if (VoxelScheme[x][y][z] == 1)
					{
						if (Selected)
						glColor3f(TextureScheme[x][y][z][0]+0.2, TextureScheme[x][y][z][1]+0.2, TextureScheme[x][y][z][2]+0.2);
						else
						glColor3f(TextureScheme[x][y][z][0], TextureScheme[x][y][z][1], TextureScheme[x][y][z][2]);
						if (VoxelScheme[x][y][z+1] != 1 || z == 1)
						{
							glVertex3f(x+0.5f, y+0.5f, z+0.5f);
							glVertex3f(x+0.5f, y-0.5f, z+0.5f);
							glVertex3f(x-0.5f, y-0.5f, z+0.5f);
							glVertex3f(x-0.5f, y+0.5f, z+0.5f);
						}
						if (VoxelScheme[x+1][y][z] != 1 && x < -xPl)
						{
							if (Selected)
							glColor3f(TextureScheme[x][y][z][0]+0.1, TextureScheme[x][y][z][1]+0.1, TextureScheme[x][y][z][2]+0.1);
							else
							glColor3f(TextureScheme[x][y][z][0]-0.1, TextureScheme[x][y][z][1]-0.1, TextureScheme[x][y][z][2]-0.1);
							glVertex3f(x+0.5f, y-0.5f, z+0.5f);
							glVertex3f(x+0.5f, y+0.5f, z+0.5f);
							glVertex3f(x+0.5f, y+0.5f, z-0.5f);
							glVertex3f(x+0.5f, y-0.5f, z-0.5f);
						}
						if (VoxelScheme[x-1][y][z] != 1 && x > -xPl)
						{
							if (Selected)
							glColor3f(TextureScheme[x][y][z][0]+0.3, TextureScheme[x][y][z][1]+0.3, TextureScheme[x][y][z][2]+0.3);
							else
							glColor3f(TextureScheme[x][y][z][0]+0.1, TextureScheme[x][y][z][1]+0.1, TextureScheme[x][y][z][2]+0.1);
							glVertex3f(x-0.5f, y+0.5f, z+0.5f);
							glVertex3f(x-0.5f, y-0.5f, z+0.5f);
							glVertex3f(x-0.5f, y-0.5f, z-0.5f);
							glVertex3f(x-0.5f, y+0.5f, z-0.5f);
						}
						/*if (VoxelScheme[x+1][y+2][z+1] != 1)
						{
							glVertex3f(x+0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y+0.5f-1.0f, z-0.5f);
							glVertex3f(x+0.5f-1.0f, y+0.5f-1.0f, z-0.5f);
						}*/
						if (VoxelScheme[x][y-1][z] != 1)
						{
							if (Selected)
							glColor3f(TextureScheme[x][y][z][0]+0.1, TextureScheme[x][y][z][1]+0.1, TextureScheme[x][y][z][2]+0.1);
							else
							glColor3f(TextureScheme[x][y][z][0]-0.1, TextureScheme[x][y][z][1]-0.1, TextureScheme[x][y][z][2]-0.1);
							glVertex3f(x-0.5f, y-0.5f, z+0.5f);
							glVertex3f(x+0.5f, y-0.5f, z+0.5f);
							glVertex3f(x+0.5f, y-0.5f, z-0.5f);
							glVertex3f(x-0.5f, y-0.5f, z-0.5f);
						}
					}
				}
			}
		}
		glEnd();
		glTranslatef(-X, -Y, -Z);
	}
};