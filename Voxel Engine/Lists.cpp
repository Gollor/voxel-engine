#include"Libraries.h"
#include"Extern.h"
#include"Spark.h"
#include"Lists.h"

SPARKLIST SparkList;
BULLETLIST BulletList;
UNITLIST UnitListT1;
UNITLIST UnitListT2;
UNITLIST UnitListSelect;

void OE_NewSpark(float X, float Y, float Z, float Xmove, float Ymove, float Zmove)
{	
	Spark* NewSpark = new Spark(X, Y, Z, Xmove, Ymove, Zmove);
	SparkList.push_front(NewSpark);
}
void OE_SparksDraw()
{
	Spark *pSpark;
	SparkListIterator iterator = SparkList.begin(); 
	while(iterator != SparkList.end())
	{
		pSpark = *iterator;
        glPushMatrix();
		pSpark->Draw();
        glPopMatrix();
		iterator++;
	}
}
void OE_SparksMove()
{
	Spark *pSpark;
	SparkListIterator iterator = SparkList.begin(); 
	while(iterator != SparkList.end())
	{
		pSpark = *iterator;
		if(pSpark->Move()==1)
		{
			delete pSpark;
			iterator = SparkList.erase(iterator);   
		}
		else
		iterator++;
	}
}

void OE_NewBullet(float X, float Y, float Z, float XYangle, float Zvector, float Force, int Damage, int Team, int SmokeType)
{	
	Bullet* NewBullet = new Bullet(X, Y, Z, XYangle, Zvector, Force, Damage, Team, SmokeType);
	BulletList.push_front(NewBullet);
}
void OE_BulletsDraw()
{
	Bullet *pBullet;
	BulletListIterator iterator = BulletList.begin(); 
	while(iterator != BulletList.end())
	{
		pBullet = *iterator;
        glPushMatrix();
		pBullet->Draw();
        glPopMatrix();
		iterator++;
	}
}
void OE_BulletsMove()
{
	Bullet *pBullet;
	BulletListIterator iterator = BulletList.begin(); 
	while(iterator != BulletList.end())
	{
		pBullet = *iterator;
		short a;
		a = pBullet->Move();
		if (a == 1)
		{
			if (pBullet->GetDamage()>=4)
			{
				int x = pBullet->GetX();
				int y = pBullet->GetY();
				OE_NewSpark(x, y, pBullet->GetZ(), 2, 2, 1);
				OE_NewSpark(x, y, pBullet->GetZ(), -2, 2, 1);
				OE_NewSpark(x, y, pBullet->GetZ(), -2, -2, 1);
				OE_NewSpark(x, y, pBullet->GetZ(), 2, -2, 1);
			}
			delete pBullet;
			iterator = BulletList.erase(iterator);
			continue;
		}
		if (a == 2)
			OE_NewSpark(pBullet->GetFloatX(),pBullet->GetFloatY(),pBullet->GetFloatZ(),0,0,-0.15);
		iterator++;
	}
}

void OE_UnitsDraw()
{
	Unit *pUnit;
	UnitListIterator iterator1 = UnitListT1.begin(); 
	while(iterator1 != UnitListT1.end())
	{
		pUnit = *iterator1;
        glPushMatrix();
		pUnit->Draw();
        glPopMatrix();
		iterator1++;
	}
	UnitListIterator iterator2 = UnitListT2.begin(); 
	while(iterator2 != UnitListT2.end())
	{
		pUnit = *iterator2;
        glPushMatrix();
		pUnit->Draw();
        glPopMatrix();
		iterator2++;
	}
}
void OE_UnitsProcess()
{
	Unit *pUnit;
	UnitListIterator iterator1 = UnitListT1.begin(); 
	while(iterator1 != UnitListT1.end())
	{
		pUnit = *iterator1;
		if(pUnit->Process()==1)
		{
			delete pUnit;
			iterator1 = UnitListT1.erase(iterator1);   
		}
		else
		iterator1++;
	}
	UnitListIterator iterator2 = UnitListT2.begin(); 
	while(iterator2 != UnitListT2.end())
	{
		pUnit = *iterator2;
		if(pUnit->Process()==1)
		{
			delete pUnit;
			iterator2 = UnitListT2.erase(iterator2);   
		}
		else
		iterator2++;
	}
}
void OE_UnitsTargeting()
{
	Unit *pUnit;

	UnitListIterator iterator1 = UnitListT1.begin(); 
	while(iterator1 != UnitListT1.end())
	{
		pUnit = *iterator1;
		pUnit->ClearTarget();
		iterator1++;
	}
	UnitListIterator iterator2 = UnitListT2.begin(); 
	while(iterator2 != UnitListT2.end())
	{
		pUnit = *iterator2;
		pUnit->ClearTarget();
		iterator2++;
	}

	UnitListIterator iterator11 = UnitListT1.begin(); 
	while(iterator11 != UnitListT1.end())
	{
		pUnit = *iterator11;
		int x = pUnit->GetX();
		int y = pUnit->GetY();
		int z = pUnit->GetZ();
		float xx = pUnit->GetXStep();
		float yy = pUnit->GetYStep();
		Unit *pUnit2;
		UnitListIterator iterator22 = UnitListT2.begin(); 
		while(iterator22 != UnitListT2.end())
		{
			pUnit2 = *iterator22;
			if (pUnit != pUnit2)
			pUnit2->Targeting(x,y,z,xx,yy);
			iterator22++;
		}
		iterator11++;
	}
	iterator11 = UnitListT2.begin(); 
	while(iterator11 != UnitListT2.end())
	{
		pUnit = *iterator11;
		int x = pUnit->GetX();
		int y = pUnit->GetY();
		int z = pUnit->GetZ();
		float xx = pUnit->GetXStep();
		float yy = pUnit->GetYStep();
		Unit *pUnit2;
		UnitListIterator iterator22 = UnitListT1.begin(); 
		while(iterator22 != UnitListT1.end())
		{
			pUnit2 = *iterator22;
			if (pUnit != pUnit2)
			pUnit2->Targeting(x,y,z,xx,yy);
			iterator22++;
		}
		iterator11++;
	}
}
void OE_BulletsCollision()
{	
	Bullet *pBullet;
	BulletListIterator iterator = BulletList.begin(); 
	while(iterator != BulletList.end())
	{
		pBullet = *iterator;
		int x = pBullet->GetX();
		int y = pBullet->GetY();
		int z = pBullet->GetZ();
		bool a = 0;
		if (pBullet->GetCounter() > 10)
		{
			Unit *pUnit;
			if (pBullet-> GetTeam() != 1)
			{
				UnitListIterator iterator2 = UnitListT1.begin(); 
				while(iterator2 != UnitListT1.end())
				{
					pUnit = *iterator2;
					int X = pUnit->GetX();
					int Y = pUnit->GetY();
					if (X+pUnit->GetXSize() >= x && x >= X && Y+pUnit->GetYSize() >= y && y >= Y && z <= pUnit->GetHeight())
					{
						a = 1;
						pUnit->Hit(x,y,pBullet->GetZ(),pBullet->GetDamage());
						if (pBullet->GetDamage()>4)
						{
							OE_NewSpark(x, y, pBullet->GetZ(), 2, 2, 1);
							OE_NewSpark(x, y, pBullet->GetZ(), -2, 2, 1);
							OE_NewSpark(x, y, pBullet->GetZ(), -2, -2, 1);
							OE_NewSpark(x, y, pBullet->GetZ(), 2, -2, 1);
						}
						break;
					}
					iterator2++;
				}
			}
			if (pBullet-> GetTeam() != 2)
			{
				UnitListIterator iterator2 = UnitListT2.begin(); 
				if (pBullet->GetCounter() > 10)
				while(iterator2 != UnitListT2.end())
				{
					pUnit = *iterator2;
					int X = pUnit->GetX();
					int Y = pUnit->GetY();
					if (X+pUnit->GetXSize() >= x && x >= X && Y+pUnit->GetYSize() >= y && y >= Y && z <= pUnit->GetHeight())
					{
						a = 1;
						pUnit->Hit(x,y,pBullet->GetZ(),pBullet->GetDamage());
						if (pBullet->GetDamage()>4)
						{
							OE_NewSpark(x, y, pBullet->GetZ(), 2, 2, 1);
							OE_NewSpark(x, y, pBullet->GetZ(), -2, 2, 1);
							OE_NewSpark(x, y, pBullet->GetZ(), -2, -2, 1);
							OE_NewSpark(x, y, pBullet->GetZ(), 2, -2, 1);
						}
						break;
					}
					iterator2++;
				}
			}
		}
		if (a == 1)
		{
			delete pBullet;
			iterator = BulletList.erase(iterator);  
		}
		else
		iterator++;
	}
}
void OE_Select(int X, int Y)
{
	Unit *pUnit;
	UnitListIterator iterator = UnitListSelect.begin(); 
	while(iterator != UnitListSelect.end())
	{
		pUnit = *iterator;
		pUnit->DisSelect();
		iterator++;
	}
	UnitListSelect.clear();
	iterator = UnitListT1.begin(); 
	while(iterator != UnitListT1.end())
	{
		pUnit = *iterator;
		int x = pUnit->GetX();
		int y = pUnit->GetY();
		if (x + 14 > X && x - 4 < X && y + 14 > Y && y - 4 < Y)
		{
			UnitListSelect.push_front(pUnit);
			pUnit->Select();
			return;
		}
		iterator++;
	}
}
void OE_Select(int X, int Y, int oldX, int oldY)
{
	int SmallX, SmallY, BigX, BigY;
	if (oldX < X) {SmallX = oldX; BigX = X;} else {SmallX = X; BigX = oldX;}
	if (oldY < Y) {SmallY = oldY; BigY = Y;} else {SmallY = Y; BigY = oldY;}

	Unit *pUnit;
	UnitListIterator iterator = UnitListSelect.begin(); 
	while(iterator != UnitListSelect.end())
	{
		pUnit = *iterator;
		pUnit->DisSelect();
		iterator++;
	}
	UnitListSelect.clear();
	iterator = UnitListT1.begin(); 
	while(iterator != UnitListT1.end())
	{
		pUnit = *iterator;
		int x = pUnit->GetX();
		int y = pUnit->GetY();
		if (x + 5 >= SmallX && x + 5 <= BigX && y + 5 >= SmallY && y + 5 <= BigY)
		{
			UnitListSelect.push_front(pUnit);
			pUnit->Select();
		}
		iterator++;
	}
}
void OE_MoveSelected(int X, int Y)
{
	Unit *pUnit;
	UnitListIterator iterator = UnitListSelect.begin(); 
	int i = 0;
	while(iterator != UnitListSelect.end())
	{
		pUnit = *iterator;
		pUnit->Move(X,Y);
		if (i == 0) {X+=15;}
		if (i == 1) {X-=15;Y+=15;}
		if (i == 2) {X-=15;Y-=15;}
		if (i == 3) {X+=15;Y-=15;}

		if (i == 4) {X+=30;Y+=15;}
		if (i == 5) {X-=30;Y+=30;}
		if (i == 6) {X-=30;Y-=30;}
		if (i == 7) {X+=30;Y-=30;}
		i++;
		iterator++;
	}
}