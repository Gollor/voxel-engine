#define __LISTS_H__
#pragma once

#include"Libraries.h"
#include"Extern.h"
#include"Spark.h"
#include"Bullet.h"
#include"Unit.h"

typedef Spark *pSpark;
typedef std::list <pSpark > SPARKLIST; 
typedef std::list<pSpark >::iterator SparkListIterator;

extern SPARKLIST SparkList;

void OE_NewSpark(float X, float Y, float Z, float Xmove, float Ymove, float Zmove);
void OE_SparksDraw();
void OE_SparksMove();

typedef Bullet *pBullet;
typedef std::list <pBullet > BULLETLIST; 
typedef std::list<pBullet >::iterator BulletListIterator;

extern BULLETLIST BulletList;

void OE_NewBullet(float X, float Y, float Z, float XYangle, float Zvector, float Force, int Damage, int Team, int SmokeType);
void OE_BulletsDraw();
void OE_BulletsMove();

typedef Unit *pUnit;
typedef std::list <pUnit > UNITLIST; 
typedef std::list<pUnit >::iterator UnitListIterator;

extern UNITLIST UnitListT1;
extern UNITLIST UnitListT2;
extern UNITLIST UnitListSelect;

void OE_UnitsDraw();
void OE_UnitsProcess();
void OE_UnitsTargeting();
void OE_BulletsCollision();

void OE_Select(int X, int Y);
void OE_Select(int X, int Y, int oldX, int oldY);
void OE_MoveSelected(int X, int Y);