#include "Libraries.h"
#include "Extern.h"
#include "Lists.h"

#pragma once

#define GL_PI 3.1415f

class Miner : public Unit
{
private:
	int Team;
	int MoveCounter, HP;
	bool VoxelScheme[10][10][10];
	float TextureScheme [10][10][10][3];
public:
	Miner(int team, int x, int y)
	{
		Team = team;
		X = x;
		Y = y;
		XStep = 0;
		YStep = 0;
		Selected = 0;
		MoveCounter = 0;
		HP = 120;
		for (int i = 80; i > 1; i--)
		{
			if (VoxelMap[X+2][Y+2][i] == 1)
			{
				Z = i+1;
				break;
			}
		}
		for (int i = 1; i < 9; i++)
		for (int j = 1; j < 9; j++)
		for (int u = 1; u < 9; u++)
		{
			VoxelScheme[i][j][u] = 1;
			TextureScheme[i][j][u][0] = 0.7;
			TextureScheme[i][j][u][1] = 0.7;
			TextureScheme[i][j][u][2] = 0.7;
		}
	}
	bool Process()
	{
		if (HP <= 0)
		{
			for (int i = X-3; i <= X+3; i+=3)
			{
				for (int y = Y-3; y <= Y+3; y+=3)
				{
					OE_NewSpark(i, y, Z+4, 2, 2, 2);
					OE_NewSpark(i, y, Z+4, -2, 2, 2);
					OE_NewSpark(i, y, Z+4, -2, -2, 2);
					OE_NewSpark(i, y, Z+4, 2, -2, 2);
				}
			}
			if (Selected)
			{	
				Unit *pUnit;
				UnitListIterator iterator1 = UnitListSelect.begin(); 
				while(iterator1 != UnitListSelect.end())
				{
					pUnit = *iterator1;
					if(pUnit==this)
					{
						iterator1 = UnitListSelect.erase(iterator1);
						return 1;
					}
					else
					iterator1++;
				}
			}
			return 1;
		}
		MoveCounter++;
		return 0;
	}
	void Move(int x, int y)
	{
	}
	void Targeting(int x, int y, int z, float xstep, float ystep)
	{
	}
	void ClearTarget()
	{
	}
	void Draw()
	{
		if (X > -xPl+150 || X < -xPl-130 || Y > -yPl+150 || Y < -10) return;
		glTranslatef(X, Y, Z);
		glBegin(GL_QUADS);
		for(int x = 1; x < 9; x++)
		{
			for (int y = 1; y < 9; y++)
			{
				for (int z = 1; z < 9; z++)
				{
					if (VoxelScheme[x][y][z] == 1)
					{
						if (Selected)
						glColor3f(TextureScheme[x][y][z][0]+0.2, TextureScheme[x][y][z][1]+0.2, TextureScheme[x][y][z][2]+0.2);
						else
						glColor3f(TextureScheme[x][y][z][0], TextureScheme[x][y][z][1], TextureScheme[x][y][z][2]);
						if (VoxelScheme[x][y][z+1] != 1 || z == 1)
						{
							glVertex3f(x+0.5f, y+0.5f, z+0.5f);
							glVertex3f(x+0.5f, y-0.5f, z+0.5f);
							glVertex3f(x-0.5f, y-0.5f, z+0.5f);
							glVertex3f(x-0.5f, y+0.5f, z+0.5f);
						}
						if (VoxelScheme[x+1][y][z] != 1 && X < -xPl)
						{
							if (Selected)
							glColor3f(TextureScheme[x][y][z][0]+0.1, TextureScheme[x][y][z][1]+0.1, TextureScheme[x][y][z][2]+0.1);
							else
							glColor3f(TextureScheme[x][y][z][0]-0.1, TextureScheme[x][y][z][1]-0.1, TextureScheme[x][y][z][2]-0.1);
							glVertex3f(x+0.5f, y-0.5f, z+0.5f);
							glVertex3f(x+0.5f, y+0.5f, z+0.5f);
							glVertex3f(x+0.5f, y+0.5f, z-0.5f);
							glVertex3f(x+0.5f, y-0.5f, z-0.5f);
						}
						if (VoxelScheme[x-1][y][z] != 1 && X > -xPl)
						{
							if (Selected)
							glColor3f(TextureScheme[x][y][z][0]+0.3, TextureScheme[x][y][z][1]+0.3, TextureScheme[x][y][z][2]+0.3);
							else
							glColor3f(TextureScheme[x][y][z][0]+0.1, TextureScheme[x][y][z][1]+0.1, TextureScheme[x][y][z][2]+0.1);
							glVertex3f(x-0.5f, y+0.5f, z+0.5f);
							glVertex3f(x-0.5f, y-0.5f, z+0.5f);
							glVertex3f(x-0.5f, y-0.5f, z-0.5f);
							glVertex3f(x-0.5f, y+0.5f, z-0.5f);
						}
						/*if (VoxelScheme[x+1][y+2][z+1] != 1)
						{
							glVertex3f(x+0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y+0.5f-1.0f, z+0.5f);
							glVertex3f(x-0.5f-1.0f, y+0.5f-1.0f, z-0.5f);
							glVertex3f(x+0.5f-1.0f, y+0.5f-1.0f, z-0.5f);
						}*/
						if (VoxelScheme[x][y-1][z] != 1)
						{
							if (Selected)
							glColor3f(TextureScheme[x][y][z][0]+0.1, TextureScheme[x][y][z][1]+0.1, TextureScheme[x][y][z][2]+0.1);
							else
							glColor3f(TextureScheme[x][y][z][0]-0.1, TextureScheme[x][y][z][1]-0.1, TextureScheme[x][y][z][2]-0.1);
							glVertex3f(x-0.5f, y-0.5f, z+0.5f);
							glVertex3f(x+0.5f, y-0.5f, z+0.5f);
							glVertex3f(x+0.5f, y-0.5f, z-0.5f);
							glVertex3f(x-0.5f, y-0.5f, z-0.5f);
						}
					}
				}
			}
		}
		glEnd();
		glTranslatef(-X, -Y, -Z);
	}
	void Hit(int x, int y, int z, int damage)
	{
		HP-=damage;
		int xx = rand() % 8 + 1;
		int yy = rand() % 8 + 1;
		int zz = rand() % 8 + 1;
		VoxelScheme[xx][yy][zz]=0;
	}
	int GetYSize()
	{
		return 10;
	}
	int GetXSize()
	{
		return 10;
	}
	int GetHeight()
	{
		return Z+10;
	}
};