#include "Libraries.h"
#include "Extern.h"

#pragma once

int ReadBmp(/*string*/char dir)
{
	FILE * pFile;
	long lSize;
	char * buffer;
	size_t result;

	
	//if (dir == '0')
	//{
		pFile = fopen ( "input.bmp" , "rb" ); 
	//} 
	/*else 
	{
		char *cstr;
		cstr = new char [dir.size()+1];
		strcpy (cstr, dir.c_str());
		pFile = fopen ( cstr , "rb" );
		delete[] cstr;  
	}*/

	if (pFile==NULL) {fputs ("File error",stderr); exit (1);}

	// obtain file size:
	fseek (pFile , 0 , SEEK_END);
	lSize = ftell (pFile);
	rewind (pFile);

	// allocate memory to contain the whole file:
	buffer = (char*) malloc (sizeof(char)*lSize);
	if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

	// copy the file into the buffer:
	result = fread (buffer,1,lSize,pFile);
	if (result != lSize) {fputs ("Reading error",stderr); exit (3);}

	/* the whole file is now loaded in the memory buffer. */
	BITMAPFILEHEADER    BMP;
	memcpy(&BMP,buffer,sizeof(BITMAPFILEHEADER));
	BITMAPV5HEADER		BMPH;
	memcpy(&BMPH,buffer+sizeof(BITMAPFILEHEADER),sizeof(BITMAPV5HEADER));
	
	// terminate
	fclose (pFile);

	if (BMPH.bV5BitCount != 24) {free (buffer);return 1;}

	/*if (dir == "0")
	{
		cout << "bfOffBits = " << BMP.bfOffBits << endl;
		cout << "bfSize = " << BMP.bfSize << endl;
		cout << "bV5Width = " << BMPH.bV5Width << endl;
		cout << "bV5Height = " << BMPH.bV5Height << endl;
	}*/

	int padding;
	padding = 4 - BMPH.bV5Width % 4;
	if (padding == 4) padding = 0;
	for (int i = 1; i < BMPH.bV5Height-1; i++)
	{
		for (int j = 1; j < BMPH.bV5Width-1; j++)
		{
			unsigned char B; memcpy(&B, buffer + (BMP.bfOffBits + padding*i + (i*BMPH.bV5Width + j)*3), 1);
			for (int u = 0; u <= B; u++) {VoxelMap[j][i][u]=1;} 
		}
	}

	/*if (dir == "0")
	{
		for (i = 0; i < BMPH.bV5Height; i++)
		{
			for (j = 0; j < BMPH.bV5Width; j++)
			{
				cout << image[j][i];
			}
			cout << endl;
		}
	}*/

	free (buffer);
	return 0;
}