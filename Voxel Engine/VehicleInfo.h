#include "Libraries.h"
#include "Extern.h"
#include "Lists.h"

#pragma once

#define GL_PI 3.1415f

class VehicleInfo : public Unit
{
private:
	char Name[10];
	int EndX, EndY, EndZ;
	bool VoxelScheme[10][10][10];
	int ValueScheme[10][10][10];
	float TextureScheme[10][10][10][3];
	float GunScheme[7][2];
	float Speed;
	int TurretHeight;
	int HP;
public:
	VehicleInfo(char name[10])
	{
		for (int i = 0; i < 10; i++)
			Name[i] = name[i];
	}
	void SetInfo(int endX, int endY, int endZ, bool voxelScheme[10][10][10], int valueScheme[10][10][10], float textureScheme [10][10][10][3], int gunScheme[7][2], int speed, int turretheight, int hp)
	{
		EndX = endX;
		EndY = endY;
		EndZ = endZ;
		for (int a = 0; a < 10; a++) {
		for (int b = 0; b < 10; b++) {
		for (int c = 0; c < 10; c++) {
		VoxelScheme[a][b][c] = voxelScheme[a][b][c];
		ValueScheme[a][b][c] = valueScheme[a][b][c];
		TextureScheme[a][b][c][0] = textureScheme[a][b][c][0];
		TextureScheme[a][b][c][1] = textureScheme[a][b][c][1];
		TextureScheme[a][b][c][2] = textureScheme[a][b][c][2]; 
		} } }
		for (int i = 0; i < 7; i++)
		{
			GunScheme[i][0] = gunScheme[i][0]; //0 - X��������	1 - Y��������	2 - Z��������	3 - ����	4 - �������	5 - �����������	6 - ��������������������*100
			GunScheme[i][1] = gunScheme[i][1];
		}
		Speed = speed;
		TurretHeight = turretheight;
		HP = hp;
	}
	int GetEndX()
	{
		return EndX;
	}
	int GetEndY()
	{
		return EndY;
	}
	int GetEndZ()
	{
		return EndZ;
	}
	int GetSpeed()
	{
		return Speed;
	}
	int GetTurretHeight()
	{
		return TurretHeight;
	}
	int GetHP()
	{
		return HP;
	}
}