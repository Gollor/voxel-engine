#include "Libraries.h"
#include "Extern.h"
#include "Unit.h"
#include "Tank.h"
#include "Infantry.h"
#include "Mech.h"
#include "Miner.h"
#include "Spark.h"
#include "Lists.h"
#include "SimpleVehicle.h"

using std::cout;
using namespace std;


#define GL_PI 3.1415f

#pragma comment(lib, "glew32.lib")

#define KEY_PLACETANK 0 
#define KEY_SELECTUNIT 1
#define KEY_MOVEUNIT 2
#define KEY_RETURNTOBUFFER 3

/////////���������� ���������� ����������////////////////////////////////////////////////////////////

bool VoxelMap[400][400][200];
bool MoveMap[400][400];
float TextureMap[400][400][200][3];
GLfloat xPl = -20.0f;
GLfloat yPl = -20.0f;
GLfloat zPl = -77.0f;
int WayEnd;
short Way[1000][2];
int MapX = 400, MapY = 400;
int Money = 0;

/////////���������� ����������///////////////////////////////////////////////////////////////////////

//GLfloat vertices[] = 0;
int MEoldX, MEoldY, BufX, BufY;
int DrawSpeed;
bool VoxelTankScheme[10][10][10];
float TextureTankScheme[10][10][10][3];
int W, H;
GLfloat xRot = 330.0f;
GLfloat yRot = 0.0f;
GLfloat zRot = 0.0f;
float FPS;
GLfloat fLowLight[] = { 0.2f, 0.1f, 0.1f, 1.0f };

float DrawData[600000];
unsigned long long DrawDataEnd;
float DrawColorData[450000];
unsigned long long DrawColorDataEnd;

/////////���������� �������//////////////////////////////////////////////////////////////////////////

void Declarations();
void GenerateTerrain();
void GenerateMoveMap();
void SetupRender();
void KeyboardKeys(unsigned char key, int x, int y);
void SpecialKeys(int key, int x, int y);
void ProcessMouse(int button, int state, int x, int y);
void ProcessMouseMotion(int x, int y);
void ChangeSize(int w, int h);
void Process(int value);
void RenderScene(void);
void RenderBlocks();
void DrawVoxel(int x, int y, int z);
void TimerFunction(int value);
void CalculateFrameRate();
void RayTracing(float x, float y, short key);

/////////�������� �������////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
	srand(time(NULL));

	Declarations();

	glutInit(&argc, argv);	
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(1280, 720);
	glutCreateWindow("Voxel Engine");
	glewInit();
	glutReshapeFunc(ChangeSize);
	glutKeyboardFunc(KeyboardKeys);	
	glutSpecialFunc(SpecialKeys);
	glutDisplayFunc(RenderScene);
    glutMouseFunc(ProcessMouse);
	glutMotionFunc(ProcessMouseMotion);
    glutTimerFunc(1, TimerFunction, 1);
    //glutTimerFunc(10, Process, 1);
	
	SetupRender();

	glutMainLoop();

	return EXIT_SUCCESS;
}

/////////�������� �������///////////////////////////////////////////////////////////////////////////

void Declarations()
{
	DrawSpeed = 1;

	for (int x = 0; x < 10; x++)
	{
		for (int y = 0; y < 10; y++)
		{
			for (int z = 0; z < 10; z++)
			{
				VoxelTankScheme[x][y][z] = 0;
			}
		}
	}
	//Hull
	for (int x = 2; x < 8; x++)
	{
		for (int y = 0; y < 10; y++)
		{
			for (int z = 0; z < 4; z++)
			{
				if (y - z >= -1)
				{
					VoxelTankScheme[x][y][z] = 1;
					float a;
					if (x == 2 || x == 7 || y < 4 || y == 9 || z == 0 || z == 3) a = 1.0f; else a = 3.0f;
					TextureTankScheme[x][y][z][0] = 0;
					TextureTankScheme[x][y][z][1] = (rand() % 100 + 155) / (255.0f*a);
					TextureTankScheme[x][y][z][2] = 0;
				}
			}
		}
	}
	//Turret
	for (int x = 3; x < 7; x++)
	{
		for (int y = 4; y < 8; y++)
		{
			for (int z = 4; z < 8; z++)
			{
				VoxelTankScheme[x][y][z] = 1;
				float a;
				if (x == 3 || x == 6 || y == 4 || y == 7 || z == 4 || z == 7) a = 1.0f; else a = 3.0f;
				TextureTankScheme[x][y][z][0] = 0;
				TextureTankScheme[x][y][z][1] = (rand() % 100 + 100) / (255.0f*a);
				TextureTankScheme[x][y][z][2] = 0;
			}
		}
	}
	//Gun
	for (int y = 0; y < 4; y++)
	{
		VoxelTankScheme[5][y][5] = 1;
		TextureTankScheme[5][y][5][0] = 0.1f;
		TextureTankScheme[5][y][5][1] = 0.1f;
		TextureTankScheme[5][y][5][2] = 0.1f;
		VoxelTankScheme[5][y][6] = 1;
		TextureTankScheme[5][y][6][0] = 0.1f;
		TextureTankScheme[5][y][6][1] = 0.1f;
		TextureTankScheme[5][y][6][2] = 0.1f;
		VoxelTankScheme[4][y][5] = 1;
		TextureTankScheme[4][y][5][0] = 0.1f;
		TextureTankScheme[4][y][5][1] = 0.1f;
		TextureTankScheme[4][y][5][2] = 0.1f;
		VoxelTankScheme[4][y][6] = 1;
		TextureTankScheme[4][y][6][0] = 0.1f;
		TextureTankScheme[4][y][6][1] = 0.1f;
		TextureTankScheme[4][y][6][2] = 0.1f;
	}
	
	for (int x = 0; x < MapX; x++)
	{
		for (int y = 0; y < MapY; y++)
		{
			for (int z = 0; z < 60; z++)
			{
				VoxelMap[x][y][z] = 0;
			}
		}
	}
	
	for (int x = 1; x < MapX-1; x++)
	{
		for (int y = 1; y < MapY-1; y++)
		{
			for (int z = 0; z < 20; z++)
			{
				VoxelMap[x][y][z] = 1;
			}
			for (int z = 1; z <= 80; z++)
			{
				TextureMap[x][y][z][0] = z/80.0f;
				TextureMap[x][y][z][1] = z/50.0f;
				TextureMap[x][y][z][2] = z/80.0f;
			}
		}
	}

	GenerateTerrain();
	//GenerateMoveMap();

	for (int x = 3; x < 7; x++)
	{
		for (int y = 4; y < 8; y++)
		{
			TextureTankScheme[x][y][7][0] = 0;
			TextureTankScheme[x][y][7][1] = 0;
			TextureTankScheme[x][y][7][2] = 1.0;
		}
	}
	
	int GunScheme[7][2];
	for (int i = 0; i < 2; i++)
	{
		GunScheme[0][i] = rand() % 10;
		GunScheme[1][i] = rand() % 10;
		GunScheme[2][i] = rand() % 6 + 4;
		GunScheme[3][i] = rand() % 12 + 2;
		GunScheme[4][i] = rand() % 2;
		GunScheme[5][i] = rand() % 30 + 10;
		GunScheme[6][i] = rand() % 10;
	}
	
	int ValueScheme[10][10][10];
	for (int x = 0; x < 10; x++){
		for (int y = 0; y < 10; y++){
			for (int z = 0; z <= 10; z++){
				ValueScheme[x][y][z]=rand()%11;
			}}}
	//SimpleVehicle* SV1 = new SimpleVehicle(1,40,40,"TEST",10,10,10,VoxelTankScheme,ValueScheme,TextureTankScheme,GunScheme,1,4,rand()%50+40);
	//UnitListT1.push_front(SV1);
	Tank* Tank1 = new Tank(1,30,30,VoxelTankScheme,TextureTankScheme);
	UnitListT1.push_front(Tank1);
	Tank* Tank2 = new Tank(1,30,45,VoxelTankScheme,TextureTankScheme);
	UnitListT1.push_front(Tank2);
	Tank* Tank3 = new Tank(1,45,30,VoxelTankScheme,TextureTankScheme);
	UnitListT1.push_front(Tank3);
	//Miner* Miner1 = new Miner(1,80,80);
	//UnitListT1.push_front(Miner1);


	for (int x = 3; x < 7; x++)
	{
		for (int y = 4; y < 8; y++)
		{
			TextureTankScheme[x][y][7][0] = 1.0;
			TextureTankScheme[x][y][7][1] = 0;
			TextureTankScheme[x][y][7][2] = 0;
		}
	}

	Tank* Tank6 = new Tank(2,390,390,VoxelTankScheme,TextureTankScheme);
	UnitListT2.push_front(Tank6);
	Tank* Tank7 = new Tank(2,380,390,VoxelTankScheme,TextureTankScheme);
	UnitListT2.push_front(Tank7);
	Tank* Tank8 = new Tank(2,390,380,VoxelTankScheme,TextureTankScheme);
	UnitListT2.push_front(Tank8);

	
	for (int y = 0; y < 6; y++)
	{
		Mech* Mech1 = new Mech(1,80,60);
		UnitListT1.push_front(Mech1);
		Mech* Mech2 = new Mech(2,350,350);
		UnitListT2.push_front(Mech2);
		Infantry* Infantry1 = new Infantry(60,80,1);
		UnitListT1.push_front(Infantry1);
		Infantry* Infantry2 = new Infantry(350,350,2);
		UnitListT2.push_front(Infantry2);
	}
}
void GenerateTerrain()
{
	/*for (int i = 0; i < MapX*MapY/100; i++)
	{
		int size = rand() % 9 + 3;
		//if (rand() % 500 == 0) size = 20;
		size = size*size;
		//int fill = rand() % 2;
		int fill = 1;
		int x = rand() % (MapX-30) + 15;
		int y = rand() % (MapY-30) + 15;
		for (int xx = x - 20; xx < x + 20; xx++)
		{
			for (int yy = y - 20; yy < y + 20; yy++)
			{
				for (int zz = 20; zz < 30; zz++)
				{
					int X = abs(xx - x);
					int Y = abs(yy - y);
					int Z = abs(20 - zz);
					if (X*X+Y*Y+Z*Z*6 < size)
					{
						VoxelMap[xx][yy][zz-1] = fill;
					}
				}
			}
		}
	}*/
	ReadBmp(0);
}
void GenerateMoveMap()
{
	for (int x = 6; x < MapX-6; x++)
	{
		for (int y = 6; y < MapY-6; y++)
		{
			int a = 0;
			for (int xx = -5; xx < 5; xx++)
			{
				for (int yy = -5; yy < 5; yy++)
				{
					if (VoxelMap[x+xx][y+yy][19]==1) a++;
					if (VoxelMap[x+xx][y+yy][20]==1) a++;
				}
			}
			/*if (a > 20 && a < 30)*/ MoveMap[x][y]=1;
		}
	}
}
void SetupRender()
{
	// Black background
	glClearColor(fLowLight[0], fLowLight[1], fLowLight[2], fLowLight[3]);

	glShadeModel(GL_FLAT);
	glFrontFace(GL_CW);

	glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);     

	glEnableClientState (GL_VERTEX_ARRAY);  
	glEnableClientState(GL_COLOR_ARRAY);
}
void KeyboardKeys(unsigned char key, int x, int y)
{
	if(key == 'w'){
		xPl -= 5.0f*sin(zRot*GL_PI/180.0f);
		yPl -= 5.0f*cos(zRot*GL_PI/180.0f);}
	if(key == 's'){
		xPl += 5.0f*sin(zRot*GL_PI/180.0f);
		yPl += 5.0f*cos(zRot*GL_PI/180.0f);}
	if(key == 'a'){
		xPl -= 5.0f*sin((zRot+270)*GL_PI/180.0f);
		yPl -= 5.0f*cos((zRot+270)*GL_PI/180.0f);}
	if(key == 'd'){
		xPl -= 5.0f*sin((zRot+90)*GL_PI/180.0f);
		yPl -= 5.0f*cos((zRot+90)*GL_PI/180.0f);}
	if (xPl < -MapX+5) xPl = -MapX+5;
	if (xPl > -5) xPl = -5;
	if (yPl < -MapY+5) yPl = -MapY+5;
	if (yPl > -5) yPl = -5;
}
void SpecialKeys(int key, int x, int y)
{
	/*if(key == GLUT_KEY_UP){
		if (zPl < - 150.0f){
		zPl += 5.0f;
		xPl -= 5.0f*sin(zRot*GL_PI/180.0f);
		yPl -= 5.0f*cos(zRot*GL_PI/180.0f);}}

	if(key == GLUT_KEY_DOWN){
		zPl -= 5.0f;
		xPl += 5.0f*sin(zRot*GL_PI/180.0f);
		yPl += 5.0f*cos(zRot*GL_PI/180.0f);}*/

	/*if(key == GLUT_KEY_LEFT)
		zRot -= 5.0f;

	if(key == GLUT_KEY_RIGHT)
		zRot += 5.0f;*/

	if(zRot > 356.0f)
		zRot = 0.0f;

	if(zRot < -1.0f)
		zRot = 355.0f;

	// Refresh the Window
	glutPostRedisplay();
}
void ProcessMouse(int button, int state, int x, int y)
{
    if(button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
    {
		RayTracing(x, y, KEY_PLACETANK);
    }
	if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
    {
		RayTracing(x, y, KEY_MOVEUNIT);
    }
    if(button == GLUT_LEFT_BUTTON)
    {
		if(state == GLUT_DOWN)
		{
			MEoldX = x, MEoldY = y;
		}
		if(state == GLUT_UP)
		{
			if (abs(MEoldX-x)+abs(MEoldY-y)>16)
			{
				RayTracing(x, y, KEY_RETURNTOBUFFER);
				int aX = BufX; int aY = BufY;
				RayTracing(MEoldX, MEoldY, KEY_RETURNTOBUFFER);
				OE_Select(aX,aY,BufX,BufY);
			}
			else
				RayTracing(MEoldX, MEoldY, KEY_SELECTUNIT);
		}
    }
}
void ProcessMouseMotion(int x, int y)
{
}
void ChangeSize(int w, int h)
{
	W = w;
	H = h;
	
	GLfloat fAspect;

    // Prevent a divide by zero
    if(h == 0)
        h = 1;

    // Set Viewport to window dimensions
    glViewport(0, 0, w, h);

    fAspect = (GLfloat)w/(GLfloat)h;

    // Reset coordinate system
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Produce the perspective projection
    gluPerspective(60.0f, fAspect, 1.0, 800.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();    
}
void Process(int value)
{
	OE_UnitsTargeting();
	OE_UnitsProcess();

	//if (rand() % 5 == 0)
	//	OE_NewBullet(rand()%380+10,rand()%380+10,88,0,0,0,15,0,1);

	OE_SparksMove();
	OE_BulletsMove();
	OE_BulletsCollision();
    //glutTimerFunc(40, Process, 1);
}
void RenderScene()
{
	Process(0);
	
	static int DrawCounter = 0;
	DrawCounter++;

	static int enemycounter = 0;
	enemycounter++;
	if (enemycounter >= 100)
	{
		Infantry* Infantry2 = new Infantry(370,370,2);
		UnitListT2.push_front(Infantry2);
		enemycounter = 0;
	}

	if (DrawCounter >= DrawSpeed)
	{
		DrawCounter = 0;

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glPushMatrix();
		glRotatef(xRot, 1.0f, 0.0f, 0.0f);
		glRotatef(yRot, 0.0f, 1.0f, 0.0f);
		glRotatef(zRot, 0.0f, 0.0f, 1.0f);
		glTranslatef(xPl, yPl, zPl);

		for (unsigned long long i = 300000; i < 600000; i++) DrawData[i] = 0;
		DrawDataEnd = 0;
		for (unsigned long long i = 100000; i < 450000; i++) DrawColorData[i] = 0;
		DrawColorDataEnd = 0;
	
		int x1 = -xPl - 100;
		if (x1 <= 0) x1 = 1;
		int x2 = -xPl + 100;
		if (x2 >= MapX-1) x2 = MapX-2;
		int y1 = -yPl;
		int y2 = -yPl + 100;
		if (y2 >= MapY-1) y2 = MapY-2;
		for (int z = 80; z >= 3; z--)
		{
			for (int x = x1; x <= x2; x++)
			{
				for (int y = y1; y <= y2; y++)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						if (abs(x+xPl)-(y+yPl)/2<=52)
							if (y+yPl+z <= 120)
						DrawVoxel(x,y,z);
					}
				}
			}
		}

		glVertexPointer(3, GL_FLOAT, 0, DrawData);
		glColorPointer(3, GL_FLOAT, 0, DrawColorData);
		glDrawArrays(GL_QUADS, 0, DrawDataEnd/3);

		OE_UnitsDraw();
		OE_SparksDraw();
		OE_BulletsDraw();

		glPopMatrix();

		glutSwapBuffers(); 
	}

    CalculateFrameRate();  
}
void RenderBlocks()
{
	//OBSOLETE
	/*
	glBegin(GL_QUADS);
	
	int x1 = -xPl - 160; if (x1 < 1) x1 = 1;
	int x11 = -xPl - 80; if (x11 < 1) x11 = 1;
	int x2 = -xPl + 160; if (x2 >= MapX) x2 = MapX-1;
	int x22 = -xPl + 80; if (x22 >= MapX) x22 = MapX-1;
	int y1 = -yPl - 160; if (y1 < 1) y1 = 1; 
	int y11 = -yPl - 80; if (y11 < 1) y11 = 1; 
	int y2 = -yPl + 160; if (y2 >= MapY) y2 = MapY-1; 
	int y22 = -yPl + 80; if (y22 >= MapY) y22 = MapY-1; 

	//000X
	//0000
	//0000
	//0000
	if (zRot >= 345 || zRot <= 115)
	{
		bool front = 0; if (zRot >= 0 && zRot <= 90) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x2; x >= x22; x--)
			{
				for (int y = y2; y >= y22; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//000X
	//0000
	//0000
	if (zRot >= 0 && zRot <= 145)
	{
		bool front = 0; if (zRot >= 0 && zRot <= 90) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x2; x >= x22; x--)
			{
				for (int y = y22; y >= -yPl; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//00X0
	//0000
	//0000
	//0000
	if (zRot >= 305 || zRot <= 90)
	{
		bool front = 0; if (zRot >= 0 && zRot <= 90) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x22; x >= -xPl; x--)
			{
				for (int y = y2; y >= y22; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//00X0
	//0000
	//0000
	if (zRot >= 290 || zRot <= 160)
	{
		bool front = 0; if (zRot >= 0 && zRot <= 90) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x22; x >= -xPl; x--)
			{
				for (int y = y22; y >= -yPl; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//0000
	//000X
	//0000
	if (zRot >= 35 || zRot <= 180)
	{
		bool front = 0; if (zRot >= 90 && zRot <= 180) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x2; x >= x22; x--)
			{
				for (int y = -yPl; y >= y11; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//0000
	//0000
	//000X
	if (zRot >= 75 && zRot <= 205)
	{
		bool front = 0; if (zRot >= 90 && zRot <= 180) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x2; x >= x22; x--)
			{
				for (int y = y11; y >= y1; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//0000
	//00X0
	//0000
	if (zRot >= 20 && zRot <= 250)
	{
		bool front = 0; if (zRot >= 90 && zRot <= 180) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x22; x >= -xPl; x--)
			{
				for (int y = -yPl; y >= y11; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//0000
	//0000
	//00X0
	if (zRot >= 90 && zRot <= 235)
	{
		bool front = 0; if (zRot >= 90 && zRot <= 180) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x22; x >= -xPl; x--)
			{
				for (int y = y11; y >= y1; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//0000
	//X000
	//0000
	if (zRot >= 180 && zRot <= 325)
	{
		bool front = 0; if (zRot >= 180 && zRot <= 270) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x11; x >= x1; x--)
			{
				for (int y = -yPl; y >= y11; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//0000
	//0000
	//X000
	if (zRot >= 165 && zRot <= 295)
	{
		bool front = 0; if (zRot >= 180 && zRot <= 270) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x11; x >= x1; x--)
			{
				for (int y = y11; y >= y1; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//0000
	//0X00
	//0000
	if (zRot >= 110 && zRot <= 340)
	{
		bool front = 0; if (zRot >= 180 && zRot <= 270) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = -xPl; x >= x11; x--)
			{
				for (int y = -yPl; y >= y11; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//0000
	//0000
	//0X00
	if (zRot >= 125 || zRot <= 270)
	{
		bool front = 0; if (zRot >= 180 && zRot <= 270) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = -xPl; x >= x11; x--)
			{
				for (int y = y11; y >= y1; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//X000
	//0000
	//0000
	//0000
	if (zRot >= 255 || zRot <= 25)
	{
		bool front = 0; if (zRot >= 270 && zRot <= 360) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x11; x >= x1; x--)
			{
				for (int y = y2; y >= y22; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//X000
	//0000
	//0000
	if (zRot >= 215 || zRot == 0)
	{
		bool front = 0; if (zRot >= 270 && zRot <= 360) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = x11; x >= x1; x--)
			{
				for (int y = y22; y >= -yPl; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0X00
	//0000
	//0000
	//0000
	if (zRot >= 270 || zRot <= 55)
	{
		bool front = 0; if (zRot >= 270 && zRot <= 360) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = -xPl; x >= x11; x--)
			{
				for (int y = y2; y >= y22; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	//0000
	//0X00
	//0000
	//0000
	if (zRot >= 200 || zRot <= 70)
	{
		bool front = 0; if (zRot >= 270 && zRot <= 360) front = 1;
		for (int z = 50; z >= 19; z--)
		{
			for (int x = -xPl; x >= x11; x--)
			{
				for (int y = y22; y >= -yPl; y--)
				{
					if (VoxelMap[x][y][z] == 1)
					{
						DrawVoxel(x,y,z,front);
					}
				}
			}
		}
	}
	glEnd();*/
}
void DrawVoxel(int x, int y, int z)
{
	if (VoxelMap[x][y][z+1] != 1 || (VoxelMap[x+1][y][z] != 1 && x < -xPl) || (VoxelMap[x-1][y][z] != 1 && x > -xPl) || VoxelMap[x][y-1][z] != 1) {} else {return;}

	//vertices = malloc(sizeof(vertices)+24);
	/*GLfloat vertices[] =		{	x-0.5,	y+0.5,	z-0.5,
									x+0.5,	y+0.5,	z-0.5,
									x-0.5,	y+0.5,	z+0.5,
									x+0.5,	y+0.5,	z+0.5,
									x-0.5,	y-0.5,	z-0.5,
									x+0.5,	y-0.5,	z-0.5,
									x-0.5,	y-0.5,	z+0.5,
									x+0.5,	y-0.5,	z+0.5};
	glVertexPointer (3, GL_FLOAT, 0, vertices);
	string Indices;

	static char frontIndices[4] = {'5'-'0', '4'-'0', '6'-'0', '7'-'0'};
	static char rightIndices[4] = {'3'-'0', '1'-'0', '5'-'0', '7'-'0'};
	static char leftIndices[4] = {'2'-'0', '7'-'0', '4'-'0', '0'-'0'};
	static char topIndices[4] = {'2'-'0', '3'-'0', '7'-'0', '6'-'0'};*/

	if (VoxelMap[x][y][z+1] != 1)
	{
		DrawData[DrawDataEnd] = x-0.5; DrawData[DrawDataEnd+1] = y+0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x+0.5; DrawData[DrawDataEnd+1] = y+0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x+0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x-0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]; DrawColorDataEnd+=3;
		//Indices.append(topIndices);
		//glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, topIndices);
	}
	if (VoxelMap[x+1][y][z] != 1 && x < -xPl)
	{
		DrawData[DrawDataEnd] = x+0.5; DrawData[DrawDataEnd+1] = y+0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]-0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]-0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]-0.1f; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x+0.5; DrawData[DrawDataEnd+1] = y+0.5; DrawData[DrawDataEnd+2] = z-0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]-0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]-0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]-0.1f; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x+0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z-0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]-0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]-0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]-0.1f; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x+0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]-0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]-0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]-0.1f; DrawColorDataEnd+=3;
		//Indices.append(rightIndices);
		//glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, rightIndices);
	}
	if (VoxelMap[x-1][y][z] != 1 && x > -xPl)
	{
		DrawData[DrawDataEnd] = x-0.5; DrawData[DrawDataEnd+1] = y+0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]+0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]+0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]+0.1f; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x-0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]+0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]+0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]+0.1f; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x-0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z-0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]+0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]+0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]+0.1f; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x-0.5; DrawData[DrawDataEnd+1] = y+0.5; DrawData[DrawDataEnd+2] = z-0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]+0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]+0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]+0.1f; DrawColorDataEnd+=3;
		//Indices.append(leftIndices);
		//glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, leftIndices);
	}
	if (VoxelMap[x][y-1][z] != 1)
	{
		DrawData[DrawDataEnd] = x+0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z-0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]+0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]+0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]+0.1f; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x-0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z-0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]+0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]+0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]+0.1f; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x-0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]+0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]+0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]+0.1f; DrawColorDataEnd+=3;
		DrawData[DrawDataEnd] = x+0.5; DrawData[DrawDataEnd+1] = y-0.5; DrawData[DrawDataEnd+2] = z+0.5; DrawDataEnd+=3;
		DrawColorData[DrawColorDataEnd] = TextureMap[x][y][z][0]+0.1f; DrawColorData[DrawColorDataEnd+1] = TextureMap[x][y][z][1]+0.1f; DrawColorData[DrawColorDataEnd+2] = TextureMap[x][y][z][2]+0.1f; DrawColorDataEnd+=3;
		//Indices.append(frontIndices);
		//glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, frontIndices);
	}
	//for (int i = 0; i < Indices.length(); i++) {cout << (int)Indices.at(i) << " ";}
	//system("PAUSE");
	//glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, Indices.c_str());
		/*
	{//OBSOLETE
		if (VoxelMap[x][y][z+1] != 1)
		{
			glColor3f(TextureMap[x][y][z][0], TextureMap[x][y][z][1], TextureMap[x][y][z][2]);
			glVertex3f(x+0.5f, y+0.5f, z+0.5f);
			glVertex3f(x+0.5f, y-0.5f, z+0.5f);
			glVertex3f(x-0.5f, y-0.5f, z+0.5f);
			glVertex3f(x-0.5f, y+0.5f, z+0.5f);
		}
		if (VoxelMap[x+1][y][z] != 1)
		{
			if (zRot > 180 || !front)
			{
				glColor3f(TextureMap[x][y][z][0]-0.1f, TextureMap[x][y][z][1]-0.1f, TextureMap[x][y][z][2]-0.1f);
				glVertex3f(x+0.5f, y-0.5f, z+0.5f);
				glVertex3f(x+0.5f, y+0.5f, z+0.5f);
				glVertex3f(x+0.5f, y+0.5f, z-0.5f);
				glVertex3f(x+0.5f, y-0.5f, z-0.5f);
			}
		}
		if (VoxelMap[x-1][y][z] != 1)
		{
			if (zRot < 180 || !front)
			{
				glColor3f(TextureMap[x][y][z][0]+0.1f, TextureMap[x][y][z][1]+0.1f, TextureMap[x][y][z][2]+0.1f);
				glVertex3f(x-0.5f, y+0.5f, z+0.5f);
				glVertex3f(x-0.5f, y-0.5f, z+0.5f);
				glVertex3f(x-0.5f, y-0.5f, z-0.5f);
				glVertex3f(x-0.5f, y+0.5f, z-0.5f);
			}
		}
		if (VoxelMap[x][y+1][z] != 1)
		{
			if ((zRot > 90 && zRot < 180) || !front)
			{
				glColor3f(TextureMap[x][y][z][0]+0.1f, TextureMap[x][y][z][1]+0.1f, TextureMap[x][y][z][2]+0.1f);
				glVertex3f(x+0.5f, y+0.5f, z+0.5f);
				glVertex3f(x-0.5f, y+0.5f, z+0.5f);
				glVertex3f(x-0.5f, y+0.5f, z-0.5f);
				glVertex3f(x+0.5f, y+0.5f, z-0.5f);
			}
		}
		if (VoxelMap[x][y-1][z] != 1)
		{
			if ((zRot < 90 || zRot > 270) || !front)
			{
				glColor3f(TextureMap[x][y][z][0]-0.1f, TextureMap[x][y][z][1]-0.1f, TextureMap[x][y][z][2]-0.1f);
				glVertex3f(x-0.5f, y-0.5f, z+0.5f);
				glVertex3f(x+0.5f, y-0.5f, z+0.5f);
				glVertex3f(x+0.5f, y-0.5f, z-0.5f);
				glVertex3f(x-0.5f, y-0.5f, z-0.5f);
			}
		}
	}*/
}
void TimerFunction(int value)
{
    // Redraw the scene with new coordinates
    glutPostRedisplay();
    glutTimerFunc(1,TimerFunction, 1);
}
void CalculateFrameRate()
{
    static float framesPerSecond = 0.0f;  
    static float lastTime = 0.0f;         
    static char strFrameRate[50] = {0};    

    float currentTime = timeGetTime() * 0.001f;

    ++framesPerSecond;

    if(currentTime - lastTime > 1.0f)
    {
        lastTime = currentTime;
        FPS=framesPerSecond;
		cout << FPS << "-";
        framesPerSecond = 0;
    }
}
void RayTracing(float x, float y, short key)
{
	float Ay = 328.0f+(y-H/2)/12.0f;
	float Az = zRot+(x-W/2)/20.0f;
	float Vz = cos(Ay*GL_PI/180);
	float Vx = sin(Az*GL_PI/180)*-sin(Ay*GL_PI/180);
	float Vy = cos(Az*GL_PI/180)*-sin(Ay*GL_PI/180);
	float r = sqrt(Vx*Vx+Vy*Vy+Vz*Vz);
	Vx = Vx/r;
	Vy = Vy/r;
	Vz = Vz/r;
	float X = -xPl + sin((zRot+270)*GL_PI/180.0f) * (W/2-x)/9.0;
	float Y = -yPl + cos((zRot+270)*GL_PI/180.0f) * (W/2-x)/9.0;
	float Z = -zPl;
	for (int i = 0; i < 1500; i++)
	{
		X+=Vx;
		Y+=Vy;
		Z-=Vz;
		if (X < 0 || X > MapX-1 || Y < 0 || Y > MapY-1 || Z < 0 || Z > 390) return;
		if (VoxelMap[(int)X][(int)Y][(int)Z]==1)
		{
			if (key == KEY_PLACETANK)
			{
				if (rand()%2==0)
				{
					for (int x = 3; x < 7; x++){
						for (int y = 4; y < 8; y++){
							TextureTankScheme[x][y][7][0] = 0;
							TextureTankScheme[x][y][7][2] = 1.0;}}
					Tank* Tank1 = new Tank(1,(int)X,(int)Y,VoxelTankScheme,TextureTankScheme);
					UnitListT1.push_front(Tank1);
				}
				else
				{
					for (int x = 3; x < 7; x++){
						for (int y = 4; y < 8; y++){
							TextureTankScheme[x][y][7][0] = 1.0;
							TextureTankScheme[x][y][7][2] = 0;}}
					Tank* Tank1 = new Tank(2,(int)X,(int)Y,VoxelTankScheme,TextureTankScheme);
					UnitListT2.push_front(Tank1);
				}
			}
			if (key == KEY_SELECTUNIT)
				OE_Select(X,Y);
			if (key == KEY_MOVEUNIT)
				OE_MoveSelected(X-5,Y-5);
			if (key == KEY_RETURNTOBUFFER)
			{
				BufX = X;
				BufY = Y;
			}
			return;
		}
	}
}